<html>
<h1>Project Time Tracker</h1>
    <h2>Content:</h2>
	<ol>
	  <li>Introduction</li>
	  <li>Requirements</li>
	  <li>Getting started</li>
	  <li>Contact</li>
	</ol>
	<p>
	======================================
	</p>
	<h3>
	1. Introduction
	</h3>
    	<p>
The "Project Time Tracker" application is a tool for recording and evaluating time spent on various projects. The functionality includes:
	</p>
	<ul>
	  <li>Creation of projects, subprojects, categories and statuses</li>
	  <li>Time recording with assignment to projects</li>
	  <li>Evaluation of the recorded times depending on the selected period, category and status</li>
	</ul>
	<p>
	======================================
	</p>
	<h3>
	2. Requirements
	</h3>
   	<p>
For the execution of the program a Java Runtime Environment (JRE) is necessary.  A current version can be downloaded at <a href="http://www.java.com/download" title="http://www.java.com/download">LINK</a>. The program uses a SQL database "PTT_database.db" created with "SQLite", which must be located in the same directory as the executable JAR file.
	</p>
	<p>
	======================================
	</p>
	<h3>
	3. Getting started
	</h3>
   	<p>
The program includes 3 windows, which are briefly explained below.
	</p>
<table>
  <tr>
    <th>Window</th>
    <th>What?</th>
    <th>How?</th>
  </tr>
  <tr>
    <td>"Project Time Tracker" (home window) <br />
     <img class="imgd" src="images/PTT_record.png" alt="" />
    </td>
    <td>Recording as well as modification/deletion of time efforts.</td>
    <td><ul>
      <li>Select date (standard = today).</li>
      <li>Press Start to write current time into Start-Field.</li>
      <li>Press Stop to write current time into Stop-Field.</li>
	<li>Enter additional information (project + status are mandatory!).</li>
      <li>Press „save“ to record in database.</li>
      <li>Click entry in the bottom to edit/delete (note: Status only can changed at „Time overview“!).</li>
    </ul></td>
   </tr>
   <tr>
     <td>"Projects/Categories/Status" (Button „Projects“ in home window)
      <img class="imgd" src="images/PTT_projects.png" alt="" />
     </td>
     <td>Manage projects and their subprojects, categories and statuses.</td>
     <td><ul>
      <li>Enter new Order No. + Description for new project or click existing project to modify/delete.</li>
      <li>To manage suborders, first click on corresponding order.</li>
      <li>To manage Categories/Status, click on the corresponding tab above.</li>
    </ul></td>
   </tr>
   <tr>
     <td>"Time Overview" (Button „Overview“ in home window)
      <img class="imgd" src="images/PTT_timeOverview.png" alt="" />
     </td>
     <td>Evaluation of recorded times.</td>
     <td><ul>
      <li>Select Start + END Date and, if necessary, category and status.</li>
      <li>Click „Search“ to display a summary of all efforts within the selected time (grouped by order and suborder).</li>
      <li>Status of efforts can be changed by selecting another entry in the corresponding cell.</li>
	<li>Click on a specific line to display all individual efforts in the lower area.</li>
    </ul></td>
   </tr>
</table>
<p>
Further hints:
</p>
<ul>
      <li><p>Pause field (window "Project Time Tracker"):</br>
	In the Pause field, minutes can be entered which are to be subtracted from the recorded time (end time minus start time). The pause time is calculated automatically when the "Start" button is pressed again after confirming the "STOP/Pause" button. The time difference between stop and start is then entered in Pause (+ any previously recorded pause times).</p>
</li>
      <li><p>Option „visible?“ (window: "Projects/Categories/Status"):</br>
	In order to keep the selection lists of orders/suborders clear, entries that are no longer required can be hidden. This is done via the checkbox "visible?". If the checkbox is removed, the corresponding order/suborder is no longer displayed in the selection lists (but still in the "Time Overview" window). If the entry is to be displayed again later, the hidden entries can be selected by removing the checkmark from "Show only visible" and accordingly the option "visible" can be checked again.</p></li>
</ul>
	<p>
	======================================
	</p>
	<h3>
	4. Contact
	</h3>
   	<p>
Please send questions or feedback to <a href="mailto:info@CodeCompounder.de" title="info@CodeCompounder.de">info@CodeCompounder.de</a>
	</p>
  </body>
</html>
