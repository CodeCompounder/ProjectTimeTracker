package persistence;

import java.awt.HeadlessException;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class DBConnector {
    String DATABASE_FILE = "PTT_database.db";
    Connection c = null;
    Statement stmt = null;
    
    public DBConnector() {
        File file = new File(DATABASE_FILE);
        if (!file.exists()) {
            System.out.println("No DB found, create new file...");
            DBCreator dbCreator = new DBCreator(this);
            dbCreator.createNewDB();
            System.out.println("New DB created.");
        }
    }

    private ResultSet dbQuery(String sql) {
        try {
            if (c == null) {
                Class.forName("org.sqlite.JDBC");
                c = DriverManager.getConnection("jdbc:sqlite:" + DATABASE_FILE);
                //System.out.println("dbQuery(String sql): Opened database successfully");
            }     

            stmt = c.createStatement();
            //System.out.println("dbQuery: " + stmt + ", " + c + " opened..." + "SQL: " + sql);
            ResultSet rs = stmt.executeQuery(sql); 
            return rs;
        } catch (Exception e) {
            System.out.println("SQL Query failed.");
            e.printStackTrace();
            ResultSet rsNull = null;
            return rsNull;
        } 
    }
    
    void executeSQL(String sql) {
        try {
            if (c == null) {
                Class.forName("org.sqlite.JDBC");
                c = DriverManager.getConnection("jdbc:sqlite:" + DATABASE_FILE);
            }    
            
            stmt = c.createStatement();
            stmt.execute("PRAGMA foreign_keys = ON;");
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            JOptionPane.showMessageDialog(null, "Update of Database failed!", "Error", JOptionPane.ERROR_MESSAGE);
            //System.exit(0);
        }
    }
    
    public ResultSet selectAllEfforts(Timestamp startDateStamp, Timestamp endDateStamp, String cat, String stat) {
        System.out.println("selectAllEfforts[" + startDateStamp + " + " + endDateStamp + "]...");
        String checkedCat = (cat == null) ? "" : " AND cat_name = '" + cat + "' ";
        String checkedStatus = (stat == null) ? "" : " AND stat_name = '" + stat + "' ";
        String sql = "SELECT sum(((strftime('%s', ef_end)-strftime('%s', ef_start))/60)-ef_pause) as sum, " +
                "main_nr, main_desc, cat_name, sub_nr, sub_desc, stat_name " + 
                "FROM effort " + 
                "LEFT JOIN mainorder ON ef_mainid = main_id " + 
                "LEFT JOIN suborder ON ef_subid = sub_id " + 
                "LEFT JOIN category ON main_catid = cat_id " + 
                "LEFT JOIN status ON ef_statid = stat_id " + 
                "WHERE date(ef_date) >= date('" + startDateStamp + "') AND date(ef_date) <= date('" + endDateStamp + "') " +
                checkedCat + checkedStatus +
                "GROUP BY ef_mainid, ef_subid, stat_name";
        //System.out.println(sql);
        return dbQuery(sql);
    }
    
    public ResultSet selectTotalTime (Timestamp dateStamp) {
        String sql = "SELECT sum(((strftime('%s', ef_end)-strftime('%s', ef_start))/60)-ef_pause) as sum "
                + "FROM effort "
                + "WHERE date(ef_date) = date('" + dateStamp + "')";
        return dbQuery(sql);
    }
    
    public ResultSet selectTotalTime (Timestamp startDateStamp, Timestamp endDateStamp, String cat, String stat) {
        String checkedCat = (cat == null) ? "" : " AND cat_name = '" + cat + "' ";
        String checkedStatus = (stat == null) ? "" : " AND stat_name = '" + stat + "' ";
        String sql = "SELECT sum(((strftime('%s', ef_end)-strftime('%s', ef_start))/60)-ef_pause) as sum, " +
                "cat_name, stat_name " + 
                "FROM effort " + 
                "LEFT JOIN mainorder ON ef_mainid = main_id " + 
                "LEFT JOIN category ON main_catid = cat_id " + 
                "LEFT JOIN status ON ef_statid = stat_id " + 
                "WHERE date(ef_date) >= date('" + startDateStamp + "') AND date(ef_date) <= date('" + endDateStamp + "') " +
                checkedCat + checkedStatus;
        System.out.println(sql);
        return dbQuery(sql);
    }
    
    public ResultSet selectEfforts(Timestamp dateStamp) {
        System.out.println("selectEfforts[" + dateStamp + "]...");
        String sql = "SELECT ef_id, ef_start, ef_end, ef_pause, "
                + "(((strftime('%s', ef_end)-strftime('%s', ef_start))/60)-ef_pause) AS sum, "
                + "main_nr, main_desc, cat_name, "
                + "sub_nr, sub_desc, stat_name, ef_comment "
                + "FROM effort LEFT JOIN (mainorder LEFT JOIN category ON main_catid = cat_id) ON ef_mainid = main_id "
                + "LEFT JOIN suborder ON ef_subid = sub_id "
                + "LEFT JOIN status ON ef_statid = stat_id "
                + "WHERE date(ef_date) = date('" + dateStamp + "') "
                + "ORDER BY ef_start ";
        return dbQuery(sql);
    }
    
    public ResultSet selectEfforts(Timestamp startDateStamp, Timestamp endDateStamp, String orderNr, String subOrderNr, String status) {
        System.out.println("selectEfforts[" + startDateStamp + " + " + endDateStamp + " + " + orderNr + "]...");
        String checkedSub = (subOrderNr == null) ? " AND ef_subid IS NULL" : " AND sub_nr = '" + subOrderNr + "'";
        String checkedStatus = (status == null) ? " " : " AND stat_name = '" + status + "' ";
        String sql = "SELECT strftime('%d.%m.%Y', ef_date) AS date, ef_start, ef_end, ef_pause, "
                + "(((strftime('%s', ef_end)-strftime('%s', ef_start))/60)-ef_pause) AS sum, "
                + "stat_name, ef_comment "
                + "FROM effort LEFT JOIN mainorder ON ef_mainid = main_id "
                + "LEFT JOIN suborder ON ef_subid = sub_id "
                + "LEFT JOIN status ON ef_statid = stat_id "
                + "WHERE date(ef_date) >= date('" + startDateStamp + "') AND date(ef_date) <= date('" + endDateStamp + "') "
                + "AND main_nr = '" + orderNr + "'" + checkedSub + checkedStatus
                + "ORDER BY date, ef_start";
        System.out.println(sql);
        return dbQuery(sql);
    }
    
    public ResultSet selectOrders(boolean visible) {
        //System.out.println("selectOrders[boolean]...");
        String sql;
        if (visible == false) {
            sql = "SELECT main_id, main_nr, main_desc, main_visible FROM mainorder " +
                    "ORDER BY main_nr";
        } else {
            sql = "SELECT main_id, main_nr, main_desc, main_visible FROM mainorder " +
                    "WHERE main_visible = 1 ORDER BY main_nr";
        }
        return dbQuery(sql);
    }
    
    public ResultSet selectOrders(int id) {
        //System.out.println("selectOrders[id]...");
        String sql;        
        sql = "SELECT main_nr, main_desc, main_visible, cat_name FROM mainorder "
                + "LEFT JOIN category ON main_catid = cat_id "
                + "WHERE main_id = " + id ;        
        return dbQuery(sql);
    }

    
    public ResultSet selectOrders(String input, boolean visible) {
        //System.out.println("selectOrders[input],[boolean]...");
        String sql;
        if (visible == false) {
            sql = "SELECT main_id, main_nr, main_desc, main_visible FROM mainorder "+
                    "WHERE main_desc LIKE '%" + input + "%' " +
                    "ORDER BY main_nr";
        } else {
            sql = "SELECT main_id, main_nr, main_desc, main_visible FROM mainorder "+
                    "WHERE main_desc LIKE '%" + input + "%' " +
                    "AND main_visible = 1 " +
                    "ORDER BY main_nr";
        }
        return dbQuery(sql);
    }
        
    public ResultSet selectOrderID(int main_nr) {
        String sql;        
        sql = "SELECT main_id FROM mainorder WHERE main_nr = " + main_nr;        
        return dbQuery(sql);
    }
    
    public ResultSet selectCategories() {
        //System.out.println("selectCategories...");
        String sql = "SELECT * FROM category ORDER BY cat_name";
        return dbQuery(sql);
    }
    
    public ResultSet selectStatus() {
        //System.out.println("selectStatuses...");
        String sql = "SELECT * FROM status";
        return dbQuery(sql);
    }
    
    public ResultSet selectSubOrders(int subID) {
        String sql = "SELECT * FROM suborder WHERE sub_id = " + subID;
        return dbQuery(sql);
    }
    
    public ResultSet selectSubOrders(String subNr, int mainID) {
        String sql = "SELECT sub_id FROM suborder WHERE "
                + "sub_nr = '" + subNr + "' AND sub_mainid = " + mainID;
        return dbQuery(sql);
    }
    
    public ResultSet selectSubOrders(boolean visible, int mainID) {
        //System.out.println("selectSubOrders[visible, id]...");
        String sql;
        if (visible == false) {
            sql = "SELECT * FROM suborder WHERE sub_mainid = " + mainID + 
                    " ORDER BY sub_nr";
        } else {
            sql = "SELECT * FROM suborder WHERE sub_mainid = " + mainID + 
                    " AND sub_visible = 1 ORDER BY sub_nr";
        }
        return dbQuery(sql);
    }
    
    public void addCategory(String catName) {
        String sql = "INSERT INTO category (cat_name) VALUES ('" + catName + "')";
        executeSQL(sql);
    }
    
    public void modifyCategory(String catNameOld, String catNameNew) {
        String sql = "UPDATE category SET cat_name = '" + catNameNew + "' " +
                            "WHERE cat_name = '" + catNameOld + "'";
        executeSQL(sql);
    }
    
    public void modifyEffort(int effortID, String startTime, String stopTime, int pauseTime, int orderID, Integer subOrderID, String comment) {
        //System.out.println("modifyEffort...");
        String sql = "UPDATE effort SET ef_start = '" + startTime + "', ef_end = '" + stopTime + "', " +
                           "ef_pause = " + pauseTime + ", ef_mainid = " + orderID + ", ef_subid = " + subOrderID + ", " +
                           "ef_comment = '" + comment + "' WHERE ef_id = " + effortID;
        
        executeSQL(sql);
    }
    
    public void modifyStatus(String statNameOld, String statNameNew) {
        String sql = "UPDATE status SET stat_name = '" + statNameNew + "' " +
                            "WHERE stat_name = '" + statNameOld + "'";
        executeSQL(sql);
    }
    
    public void deleteCategory(String catName) {
        String sql1 = "Select main_id, cat_name FROM mainorder "
                + "LEFT JOIN category ON main_catid = cat_id "
                + "WHERE cat_name = '" + catName + "'";
        
        ResultSet rsCat = dbQuery(sql1);
        ArrayList<Integer> idBuffer = new ArrayList<Integer>();
        try {
            while (rsCat.next()) {
                idBuffer.add(rsCat.getInt("main_id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        for (int i = 0 ; i < idBuffer.size() ; i++) {
            String sql2 = "UPDATE mainorder SET main_catid = NULL "
                    + "WHERE main_id = " + idBuffer.get(i);
            executeSQL(sql2);
            //System.out.println("main_id = " + idBuffer.get(i) + " set to null.");
        }
        
        String sql3 = "DELETE FROM category WHERE cat_name = '" + catName + "'";
        executeSQL(sql3);
    }
    
    public void addProject (String project, int orderNr, int visible, String category) {
        String sql = "INSERT INTO mainorder (main_desc, main_nr, main_visible, main_catid) "+ 
                                "VALUES ('" + project + "', " + orderNr + " , " + visible + ", " +
                                "(SELECT cat_id FROM category WHERE cat_name = '" + category + "'))";
        executeSQL(sql);      
    }
    
    public void addSubProject(int orderID, String subOrderNr, String subOrderDesc, int visible) {
        //System.out.println("addSubProject");
        String sql = "INSERT INTO suborder (sub_mainid, sub_nr, sub_desc, sub_visible) "+ 
                           "VALUES (" + orderID + ", '" + subOrderNr + "', '" + subOrderDesc + "' , " + visible + ")";
        executeSQL(sql);
    }
    
    public void addEffort(Timestamp dateStamp,String startTime,String stopTime,int pauseTime,int orderID,Integer subOrderID,String comment,int statusID) {
        //System.out.println("addEffort...");
        String sql = "INSERT INTO effort (ef_date, ef_start, ef_end, ef_pause, ef_mainid, ef_subid, ef_comment, ef_statid) "+ 
                           "VALUES ('" + dateStamp + "', '" + startTime + "', '" + stopTime + "' , " + pauseTime + ", " + orderID + ", " + subOrderID + ", '" + comment + "', " + statusID + ")";
        executeSQL(sql);
        //SQLite: SELECT ef_id, (((strftime('%s', ef_end)-strftime('%s', ef_start))/60)-ef_pause) as 'Difference' from effort
    }
    
    public void updateOrder(int id, String project, int orderNr, int visible, String category) {
        String sql = "UPDATE mainorder SET main_desc = '" + project + "', " +
                            "main_nr = " + orderNr + " , main_visible = " + visible + " , " +
                            "main_catid = (SELECT cat_id FROM category WHERE cat_name = '" + category + "') " + 
                            "WHERE main_id = " + id; 
        executeSQL(sql);
    }
    
    public void updateStatus(Timestamp startDateStamp, Timestamp endDateStamp, String orderNr, String subOrderNr, String oldStatus, String newStatus) {
        //System.out.println("updateStatus[" + startDateStamp + " + " + endDateStamp + " + " + orderNr + "]...");
        String checkedSub = (subOrderNr == null) ? "" : " AND sub_nr = '" + subOrderNr + "' ";
        String checkedOldStatus = (oldStatus == null) ? "" : " AND stat_name = '" + oldStatus + "' ";
        
        //Select-Befehl
        String sql1 = "SELECT ef_id "
                + "FROM effort LEFT JOIN mainorder ON ef_mainid = main_id "
                + "LEFT JOIN suborder ON ef_subid = sub_id "
                + "LEFT JOIN status ON ef_statid = stat_id "
                + "WHERE date(ef_date) >= date('" + startDateStamp + "') AND date(ef_date) <= date('" + endDateStamp + "') "
                + "AND main_nr = '" + orderNr + "'" + checkedSub + checkedOldStatus;
        //System.out.println(sql1);
        
        //Füge sql1-Befehl in Update-Anweisung sql2 ein
        String sql2 = "UPDATE effort SET ef_statid = (SELECT stat_id FROM status WHERE stat_name = '" + newStatus + "') WHERE ef_id IN (" + sql1 + ")";
        System.out.println(sql2);
        executeSQL(sql2);
    }
    
    public void updateSubOrder(int subOrderId, String subOrderNr, String subOrderDesc, int visible) {
        //System.out.println("updateSubOrder...");
        String sql = "UPDATE suborder SET sub_nr = '" + subOrderNr + "', " +
                "sub_desc = '" + subOrderDesc + "' , sub_visible = " + visible + " " + 
                "WHERE sub_id = " + subOrderId; 
        executeSQL(sql);
    }
    
    public void deleteProject(int id) {
        //System.out.println("ID = " + id);
        String sql1 = "SELECT * FROM effort WHERE ef_mainid = " + id;
        ResultSet rsQuery = dbQuery(sql1);
        try {
            if (rsQuery.next()) {
                JOptionPane.showMessageDialog(null, "Can't delete project. Delete first related efforts!", "Error", JOptionPane.INFORMATION_MESSAGE);
                return;
            }
        } catch (HeadlessException e) {
            e.printStackTrace();
            return;
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
        String sql2 = "DELETE FROM mainorder WHERE main_id = " + id;
        executeSQL(sql2);
    }
    
    public void deleteSubProject(int id) {
        //System.out.println("deleteSubProject ID = " + id);
        
        String sql1 = "SELECT * FROM effort WHERE ef_subid = " + id;
        ResultSet rsQuery = dbQuery(sql1);
        try {
            if (rsQuery.next()) {
                JOptionPane.showMessageDialog(null, "Can't delete Suborder. Delete first related efforts!", "Error", JOptionPane.INFORMATION_MESSAGE);
                return;
            }
        } catch (HeadlessException e) {
            e.printStackTrace();
            return;
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
        
        String sql2 = "DELETE FROM suborder WHERE sub_id = " + id;
        executeSQL(sql2);
    }
    
    public void deleteEffort(String id) {
        //System.out.println("deleteEffort ID = " + id);
        String sql = "DELETE FROM effort WHERE ef_id = " + id;
        executeSQL(sql);
    }
    
    public void closeDB() {
        try {
            stmt.close();
            c.close();
            //System.out.println("DB closed: " + stmt + ", " + c);
        } catch (SQLException e) {
            System.out.println("Closing Database failed.");
            e.printStackTrace();
        }
    }    
}
