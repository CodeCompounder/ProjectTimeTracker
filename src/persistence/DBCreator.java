package persistence;

import javax.swing.JOptionPane;

class DBCreator {
    private DBConnector dbConnector;

    DBCreator(DBConnector dbConnector) {
        this.dbConnector = dbConnector;
    }
    
    void createNewDB() {
        try {
            createTableCategory();
            createTableStatus();
            createTableMainorder();
            createTableSuborder();
            createTableEffort();
            initDB();
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Couldn't create Database file! Program will be terminated", "Error", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
        
    }
    
    private void createTableCategory() {
        String sql = "CREATE TABLE category ("
                + "cat_id INTEGER NOT NULL, "
                + "cat_name TEXT NOT NULL, "
                + "PRIMARY KEY(cat_id AUTOINCREMENT))";        
        dbConnector.executeSQL(sql);
    }
    
    private void createTableStatus() {
        String sql = "CREATE TABLE status ("
                + "stat_id INTEGER, "
                + "stat_name TEXT NOT NULL, "
                + "PRIMARY KEY(stat_id AUTOINCREMENT))";        
        dbConnector.executeSQL(sql);
    }
    
    private void createTableMainorder() {
        String sql = "CREATE TABLE mainorder ("
                + "main_id INTEGER NOT NULL, "
                + "main_desc TEXT NOT NULL, "
                + "main_nr INTEGER NOT NULL, "
                + "main_visible INTEGER, "
                + "main_catid INTEGER, "
                + "PRIMARY KEY(main_id AUTOINCREMENT), "
                + "FOREIGN KEY(main_catid) REFERENCES category(cat_id))";
        dbConnector.executeSQL(sql);
    }
    
    private void createTableSuborder() {
        String sql = "CREATE TABLE suborder ("
                + "sub_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "sub_mainid INTEGER NOT NULL, "
                + "sub_nr TEXT NOT NULL, "
                + "sub_desc TEXT NOT NULL, "
                + "sub_visible INTEGER, "
                + "FOREIGN KEY(sub_mainid) REFERENCES mainorder(main_id) ON DELETE CASCADE)";
        dbConnector.executeSQL(sql);
    }
    
    private void createTableEffort() {
        String sql = "CREATE TABLE effort ("
                + "ef_id INTEGER, "
                + "ef_date INTEGER NOT NULL, "
                + "ef_start INTEGER NOT NULL, "
                + "ef_end INTEGER NOT NULL, "
                + "ef_pause INTEGER, "
                + "ef_comment TEXT, "
                + "ef_mainid INTEGER, "
                + "ef_subid INTEGER, "
                + "ef_statid INTEGER, "
                + "FOREIGN KEY(ef_subid) REFERENCES suborder(sub_id) ON UPDATE SET NULL, "
                + "FOREIGN KEY(ef_statid) REFERENCES status(stat_id) ON UPDATE SET NULL, "
                + "FOREIGN KEY(ef_mainid) REFERENCES mainorder(main_id) ON UPDATE SET NULL, "
                + "PRIMARY KEY(ef_id AUTOINCREMENT))";
        dbConnector.executeSQL(sql);
    }
    
    private void initDB() {
        String sql = "INSERT INTO status (stat_name) "
                + "VALUES "
                + "('open'), "
                + "('booked'), "
                + "('recorded otherwise'), "
                + "('not bookable'), "
                + "('other')";        
        dbConnector.executeSQL(sql);
        dbConnector.addCategory("misc");
        dbConnector.addProject("misc", 100000, 1, "misc");
    }
}
