package view;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.Calendar;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.table.*;

//import org.jdatepicker.impl.*;
import net.sourceforge.jdatepicker.impl.*;

public class TimeOverview {
    private JFrame mainFrame = new JFrame("Time Overview");
    
    //NorthPanel
    private JPanel northPanel = new JPanel(new GridLayout(0,3,5,5));
    private JDatePickerImpl datePickerStart;
    private JDatePickerImpl datePickerEnd;
    private JButton searchButton;
    private JList<String> catList;
    private JList<String> statList;
    
    //CenterPanel
    private JPanel centerPanel = new JPanel(new GridLayout(0,1));
    private JPanel centerUpperPanel = new JPanel(new BorderLayout());
    private JPanel centerLowerPanel = new JPanel(new BorderLayout());
    private JTable upperTable = new JTable();  
    private JTable lowerTable = new JTable();
    
    //SouthPanel
    private JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    
    
    public TimeOverview() {
        initMainFrame();
        initNorthPanel();
        initCenterPanel();
        initSouthPanel();
        //mainFrame.pack();
    }


    private void initMainFrame() {
        mainFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        mainFrame.setLayout(new BorderLayout());
        
        try {
            //System.out.println(getClass().getResource("icon.png"));
            Image image = ImageIO.read(getClass().getResource("icon.png"));
            mainFrame.setIconImage(image);
        } catch (IOException e) {
            System.out.println("Can't find logo.");
            e.printStackTrace();
        } 
        
        mainFrame.add(northPanel, BorderLayout.NORTH);
        mainFrame.add(centerPanel, BorderLayout.CENTER);
        mainFrame.add(southPanel, BorderLayout.SOUTH);
        
        mainFrame.setSize(740,  600);     
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
    }


    private void initNorthPanel() {
        northPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        JPanel northLeftPanel = new JPanel(new GridLayout(3,0,5,5));
        northPanel.add(northLeftPanel);
        
        //Left Panel
        JPanel northLeft1Panel = new JPanel(new BorderLayout());
        northLeft1Panel.setBorder(BorderFactory.createTitledBorder("Start"));
        UtilDateModel modelStart = new UtilDateModel();
        Calendar lastMonth = Calendar.getInstance();
        lastMonth.add(Calendar.MONTH, - 1);
        modelStart.setDate(lastMonth.get(Calendar.YEAR), lastMonth.get(Calendar.MONTH), lastMonth.get(Calendar.DAY_OF_MONTH));
        modelStart.setSelected(true);
        
        /*Properties p = new Properties();
        p.put("text.today", "Today");
        p.put("text.month", "Month");
        p.put("text.year", "Year");*/
        
        JDatePanelImpl datePanelStart = new JDatePanelImpl(modelStart);
        datePickerStart = new JDatePickerImpl(datePanelStart, new DateComponentFormatter());
        datePickerStart.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        JFormattedTextField dateStartTextField = datePickerStart.getJFormattedTextField();
        dateStartTextField.setFont(new Font("Monospaced", 1, 20));
        dateStartTextField.setBackground(Color.WHITE);
        dateStartTextField.setHorizontalAlignment(JTextField.CENTER);
        northLeft1Panel.add(datePickerStart, BorderLayout.EAST);        
        northLeftPanel.add(northLeft1Panel);
        
        UtilDateModel modelEnd = new UtilDateModel();
        Calendar rightNow = Calendar.getInstance();
        modelEnd.setDate(rightNow.get(Calendar.YEAR), rightNow.get(Calendar.MONTH), rightNow.get(Calendar.DAY_OF_MONTH));
        modelEnd.setSelected(true);
        JPanel northLeft2Panel = new JPanel(new BorderLayout());
        northLeft2Panel.setBorder(BorderFactory.createTitledBorder("END"));
        JDatePanelImpl datePanelEnd = new JDatePanelImpl(modelEnd);
        datePickerEnd = new JDatePickerImpl(datePanelEnd, new DateComponentFormatter());
        datePickerEnd.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        JFormattedTextField dateEndTextField = datePickerEnd.getJFormattedTextField();
        dateEndTextField.setFont(new Font("Monospaced", 1, 20));
        dateEndTextField.setBackground(Color.WHITE);
        dateEndTextField.setHorizontalAlignment(JTextField.CENTER);
        northLeft2Panel.add(datePickerEnd, BorderLayout.EAST);  
        northLeftPanel.add(northLeft2Panel);
        
        JPanel northLeft3Panel = new JPanel(new BorderLayout());
        northLeft3Panel.setBorder(BorderFactory.createEmptyBorder(5, 10, 10, 5));
        searchButton = new JButton("Search");
        searchButton.setFont(new Font(searchButton.getFont().getFontName(), 0, 20));
        northLeft3Panel.add(searchButton);
        northLeftPanel.add(northLeft3Panel);
        
        //Center Panel
        JPanel northCenterPanel = new JPanel(new BorderLayout());
        northCenterPanel.setBorder(BorderFactory.createTitledBorder("Select Categories"));
        DefaultListModel<String> catListModel = new DefaultListModel<String>();
        catList = new JList<String>(catListModel);
        catList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane catScrollPane = new JScrollPane(catList);
        northCenterPanel.add(catScrollPane, BorderLayout.CENTER);
        northPanel.add(northCenterPanel);        
        
        //Right Panel
        JPanel northRightPanel = new JPanel(new BorderLayout());
        northRightPanel.setBorder(BorderFactory.createTitledBorder("Select Status"));
        DefaultListModel<String> statListModel = new DefaultListModel<String>();
        statList = new JList<String>(statListModel);
        statList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane statScrollPane = new JScrollPane(statList);
        northRightPanel.add(statScrollPane, BorderLayout.CENTER);
        northPanel.add(northRightPanel);
        
    }


    private void initCenterPanel() {  
        centerUpperPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(5, 5, 5, 5),
                BorderFactory.createTitledBorder("Group by Order/Suborder")));
        centerPanel.add(centerUpperPanel);
        
        //JPanel centerLowerPanel = new JPanel(new BorderLayout());
        centerLowerPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(5, 5, 5, 5),
                BorderFactory.createTitledBorder("Details")));
        centerPanel.add(centerLowerPanel);
        
        //Upper Panel        
        JScrollPane upperScrollPane = new JScrollPane(upperTable);
        upperScrollPane.setLayout(new ScrollPaneLayout());
        //upperScrollPane.setPreferredSize(upperTable.getPreferredSize());
        //centerUpperPanel.add(upperScrollPane, BorderLayout.CENTER);
        
        DefaultTableModel upperTableModel = new DefaultTableModel();
        DefaultTableColumnModel upperTableColumnModel = new DefaultTableColumnModel();
        upperTable.setModel(upperTableModel);
        upperTable.setAutoCreateRowSorter(true);
        upperTable.setColumnModel(upperTableColumnModel);
        String[] upperLabels = {"\u03a3 [hh:mm]", "OrderNr", "Project", "Category", "SubOrder", "SubProject", "Status"};
        upperTableModel.setColumnIdentifiers(upperLabels);
        TableColumn column = null;
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment( JLabel.CENTER );
        for (int i = 0; i < upperLabels.length; i++) {
            column = upperTable.getColumnModel().getColumn(i);
            column.setCellRenderer( centerRenderer );
            if (i == 2 || i == 5) {
                column.setPreferredWidth(100);
            } else if (i == 0 || i == 1) {
                column.setPreferredWidth(25);
            } else {
                column.setPreferredWidth(50);
            }
        }
        upperTable.setPreferredScrollableViewportSize(new Dimension((int)upperTable.getPreferredScrollableViewportSize().getWidth(),
                (int)upperTable.getPreferredSize().getHeight()));
        centerUpperPanel.add(upperScrollPane, BorderLayout.CENTER);
        
        //Lower Panel        
        JScrollPane lowerScrollPane = new JScrollPane(lowerTable);
        lowerScrollPane.setLayout(new ScrollPaneLayout());
        //lowerScrollPane.setPreferredSize(lowerTable.getPreferredSize());
        //centerLowerPanel.add(lowerScrollPane, BorderLayout.CENTER);
        
        DefaultTableModel lowerTableModel = new DefaultTableModel();
        DefaultTableColumnModel lowerTableColumnModel = new DefaultTableColumnModel();
        lowerTable.setModel(lowerTableModel);
        lowerTable.setAutoCreateRowSorter(true);
        lowerTable.setColumnModel(lowerTableColumnModel);
        String[] lowerLabels = {"Date", "Start", "Stop", "Pause", "\u03a3 [hh:mm]", "Status", "Comment"};
        lowerTableModel.setColumnIdentifiers(lowerLabels);
        TableColumn columnLowerTable = null;
        for (int i = 0; i < lowerLabels.length; i++) {
            columnLowerTable = lowerTable.getColumnModel().getColumn(i);
            columnLowerTable.setCellRenderer( centerRenderer );
            if (i >= 0 && i < 5) {
                columnLowerTable.setPreferredWidth(5);
            } else if (i == 5 ) {
                columnLowerTable.setPreferredWidth(50);
            } else {
                columnLowerTable.setPreferredWidth(200);
            }
        }
        lowerTable.setPreferredScrollableViewportSize(new Dimension((int)lowerTable.getPreferredScrollableViewportSize().getWidth(),
                (int)lowerTable.getPreferredSize().getHeight()));
        centerLowerPanel.add(lowerScrollPane, BorderLayout.CENTER);        
    }

    private void initSouthPanel() {
        JButton closeButton = new JButton("Close");
        southPanel.add(closeButton);
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainFrame.dispose();               
            }            
        });        
    }
    
    public void setBorderLabel(String label) {
        centerUpperPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(5, 5, 5, 5),
                BorderFactory.createTitledBorder(label)));
    }
    
    //GETTER
    public JFrame getMainFrame() {return mainFrame;}
    public JDatePickerImpl getDatePickerStart() {return datePickerStart;}
    public JDatePickerImpl getDatePickerEnd() {return datePickerEnd;}
    public JButton getSearchButton() {return searchButton;}
    public JList<String> getCatList() {return catList;}
    public JList<String> getStatList() {return statList;}
    
    //CenterPanel
    public JTable getUpperTable() {return upperTable;}  
    public JTable getLowerTable() {return lowerTable;}
    public JPanel getCenterLowerPanel() {return centerLowerPanel;}


}
