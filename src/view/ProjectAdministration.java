package view;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

public class ProjectAdministration {
    private JFrame mainFrame = new JFrame("Projects/Categories/Status");
    private JTabbedPane centerPane = new JTabbedPane();
    
    //Project Panel
    private JPanel projectPanel = new JPanel();
    private JPanel pfNorthPanel = new JPanel();
    private JTextField searchProjectTextField;
    
    //Order Field
    private JTextField orderNrTextField;
    private JTextField projectTextField;
    private JComboBox<String>categoryComboBox;
    private JCheckBox orderVisible;
    private JButton newButton = new JButton("New");
    private JButton modifyButton = new JButton("Modify");
    private JButton deleteButton = new JButton("Delete");
    
    //SubOrder Field
    private JTextField subOrderNrTextField;
    private JTextField subProjectTextField;
    private JCheckBox subOrderVisible;    
    private JButton newSubButton = new JButton("New");
    private JButton modifySubButton = new JButton("Modify");
    private JButton deleteSubButton = new JButton("Delete");
    
    private JPanel pfCenterPanel = new JPanel();
    private JCheckBox visibleBox = new JCheckBox("Show only visible?", true);
    private DefaultListModel<String> listModelOrder = new DefaultListModel<String>();
    private DefaultListModel<String> listModelSuborder = new DefaultListModel<String>();
    private JList orderList = new JList(listModelOrder);
    private JList subOrderList = new JList(listModelSuborder);
    
    private JPanel pfSouthPanel = new JPanel();    
    
    //Category Panel
    private JPanel catPanel = new JPanel();
    private JTextField catNameTextField = new JTextField("", 10);
    private JButton newCatButton = new JButton("New");
    private JButton modifyCatButton = new JButton("Modify");
    private JButton deleteCatButton = new JButton("Delete");
    DefaultListModel<String> listModelCat = new DefaultListModel<String>();
    JList catList = new JList(listModelCat);
    
    //Status Panel
    private JPanel statPanel = new JPanel(); 
    private JTextField statNameTextField = new JTextField("", 10);
    private JButton modifyStatButton = new JButton("Modify");
    DefaultListModel<String> listModelStat = new DefaultListModel<String>();
    JList statList = new JList(listModelStat);
    
    public ProjectAdministration() {
        initProjectPanel();
        initCatPanel();
        initStatPanel();        
        initMainFrame();
        mainFrame.setVisible(true);
    }
    
    private void initProjectPanel() {

        projectPanel.setLayout(new BorderLayout());
        projectPanel.add(pfNorthPanel, BorderLayout.NORTH);
        projectPanel.add(pfCenterPanel, BorderLayout.CENTER);
        projectPanel.add(pfSouthPanel, BorderLayout.SOUTH);
        
        //NorthPanel
        pfNorthPanel.setLayout(new BorderLayout());
        pfNorthPanel.setBorder(BorderFactory.createEtchedBorder());
        
        JPanel northOben = new JPanel();
        northOben.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
        pfNorthPanel.add(northOben, BorderLayout.NORTH);
        northOben.setLayout(new FlowLayout(FlowLayout.LEADING));
        JLabel searchProjectLabel = new JLabel("Search Order: ");
        northOben.add(searchProjectLabel);
        searchProjectTextField = new JTextField("", 15);
        northOben.add(searchProjectTextField);                      
        
        //Order Elements
        JPanel northMitte = new JPanel();
        northMitte.setLayout(new GridLayout(0,2,5,5));
        pfNorthPanel.add(northMitte, BorderLayout.CENTER);
        
        JPanel northMitteWest = new JPanel();        
        northMitte.add(northMitteWest);
        northMitteWest.setBorder(BorderFactory.createTitledBorder("Order"));
        northMitteWest.setLayout(new BorderLayout());
        JPanel northMitteWestOben = new JPanel();
        northMitteWestOben.setLayout(new GridLayout(0,2));
        northMitteWest.add(northMitteWestOben, BorderLayout.NORTH);
        JLabel orderNr = new JLabel("No.:");
        northMitteWestOben.add(orderNr);
        orderNrTextField = new JTextField("");        
        northMitteWestOben.add(orderNrTextField); 
        JLabel description = new JLabel("Description:            ");
        northMitteWestOben.add(description);      
        projectTextField = new JTextField("");        
        northMitteWestOben.add(projectTextField);        
        JLabel categoryLabel = new JLabel("Category");
        northMitteWestOben.add(categoryLabel);        
        categoryComboBox = new JComboBox<String>(); 
        //categoryComboBox.set
        northMitteWestOben.add(categoryComboBox);
        northMitteWestOben.add(new JLabel(""));
        orderVisible = new JCheckBox("visible?", true);
        northMitteWestOben.add(orderVisible);
        
        JPanel northMitteWestUnten = new JPanel();
        northMitteWestUnten.setLayout(new FlowLayout(FlowLayout.LEFT));
        northMitteWest.add(northMitteWestUnten, BorderLayout.SOUTH);
        northMitteWestUnten.add(newButton);
        northMitteWestUnten.add(modifyButton);
        northMitteWestUnten.add(deleteButton);
        
        //Suborder Elements
        JPanel northMitteEast = new JPanel();
        northMitte.add(northMitteEast);
        northMitteEast.setBorder(BorderFactory.createTitledBorder("Suborder"));
        northMitteEast.setLayout(new BorderLayout());
        JPanel northMitteEastOben = new JPanel();
        northMitteEastOben.setLayout(new GridLayout(0,2));
        northMitteEast.add(northMitteEastOben, BorderLayout.NORTH); 
        JLabel subOrderNr = new JLabel("No. ");
        northMitteEastOben.add(subOrderNr);
        subOrderNrTextField = new JTextField("");        
        northMitteEastOben.add(subOrderNrTextField);
        JLabel subDescription = new JLabel("Description:            ");
        northMitteEastOben.add(subDescription);      
        subProjectTextField = new JTextField("");        
        northMitteEastOben.add(subProjectTextField);
        northMitteEastOben.add(new JLabel(""));
        northMitteEastOben.add(new JLabel(""));
        northMitteEastOben.add(new JLabel(""));
        subOrderVisible = new JCheckBox("visible?", true);
        northMitteEastOben.add(subOrderVisible);
        //northMitteEastOben.add(new JLabel(""));
        
        JPanel northMitteEastUnten = new JPanel();
        northMitteEastUnten.setLayout(new FlowLayout(FlowLayout.LEFT));
        northMitteEast.add(northMitteEastUnten, BorderLayout.SOUTH);
        northMitteEastUnten.add(newSubButton);
        northMitteEastUnten.add(modifySubButton);
        northMitteEastUnten.add(deleteSubButton);
        
        //Center Panel
        pfCenterPanel.setLayout(new BorderLayout());
        
        JPanel centerUnten = new JPanel();
        pfCenterPanel.add(centerUnten, BorderLayout.SOUTH);
        //centerUnten.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
        centerUnten.setLayout(new FlowLayout(FlowLayout.LEADING));
        centerUnten.add(visibleBox);
        
        //Order Liste
        JPanel centerMitte = new JPanel();
        pfCenterPanel.add(centerMitte, BorderLayout.CENTER);
        centerMitte.setLayout(new GridLayout(0,2, 5, 5));
        
        orderList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane orderScrollPane = new JScrollPane(orderList);
        centerMitte.add(orderScrollPane);

        //SubOrder Liste  
        subOrderList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane subOrderScrollPane = new JScrollPane(subOrderList);
        centerMitte.add(subOrderScrollPane);
        
        //SouthPanel
        pfSouthPanel.setLayout(new GridLayout(0, 2));
        
        JPanel southLinks = new JPanel();
        pfSouthPanel.add(southLinks);
        southLinks.setLayout(new FlowLayout(FlowLayout.LEFT));
        
        JPanel southRechts = new JPanel();
        pfSouthPanel.add(southRechts);
        southRechts.setLayout(new FlowLayout(FlowLayout.RIGHT));
        JButton closeButton = new JButton("Close");
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainFrame.dispose();       
            }            
        });
        southRechts.add(closeButton);
    }

    private void initCatPanel() {
        catPanel.setLayout(new BorderLayout());
        JPanel catPanelOben = new JPanel();
        catPanel.add(catPanelOben, BorderLayout.NORTH);
        
        //Leeren Platz unten ausfüllen:
        JPanel catPanelBufferUnten = new JPanel();
        catPanelBufferUnten.setLayout(new FlowLayout(FlowLayout.RIGHT));
        catPanelBufferUnten.setBorder(BorderFactory.createEmptyBorder(50, 0, 0, 1));
        JButton closeButton2 = new JButton("Close");
        closeButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainFrame.dispose();         
            }            
        });
        catPanelBufferUnten.add(closeButton2);
        catPanel.add(catPanelBufferUnten, BorderLayout.SOUTH);
        
        
        
                
        catPanelOben.setLayout(new GridLayout(0,2,5,5));
        catPanelOben.setBorder(BorderFactory.createTitledBorder("Categories"));;
        
        //Kategorien linke Seite
        JPanel catPanelObenLinks = new JPanel();
        catPanelObenLinks.setLayout(new BorderLayout());
        //catPanelObenLinks.setBorder(BorderFactory.createEtchedBorder());
        catPanelOben.add(catPanelObenLinks);
        
        JPanel catPanelObenInputField = new JPanel();
        catPanelObenInputField.setLayout(new FlowLayout(FlowLayout.LEFT));
        catPanelObenLinks.add(catPanelObenInputField, BorderLayout.NORTH);
        catPanelObenInputField.add(new JLabel("Name:   "));       
        catPanelObenInputField.add(catNameTextField);
        
        JPanel catPanelObenButtonField = new JPanel();
        catPanelObenButtonField.setLayout(new FlowLayout(FlowLayout.LEFT));
        catPanelObenLinks.add(catPanelObenButtonField, BorderLayout.CENTER);
        catPanelObenButtonField.add(newCatButton);
        catPanelObenButtonField.add(modifyCatButton);
        catPanelObenButtonField.add(deleteCatButton);
        
        //Kategorien rechte Seite (Liste)
        JPanel catPanelObenRechts = new JPanel();
        catPanelObenRechts.setLayout(new BorderLayout());
        //catPanelObenLinks.setBorder(BorderFactory.createEtchedBorder());
        catPanelOben.add(catPanelObenRechts);
        
        catList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane orderScrollPane = new JScrollPane(catList);
        catPanelObenRechts.add(orderScrollPane, BorderLayout.CENTER);     
    }
    
    private void initStatPanel() {
        statPanel.setLayout(new GridLayout(0,2,5,5));
        statPanel.setBorder(BorderFactory.createTitledBorder("Status"));;
        
        //Status linke Seite
        JPanel statPanelLinks = new JPanel();
        statPanelLinks.setLayout(new BorderLayout());
        statPanel.add(statPanelLinks);
        
        JPanel statPanelInputField = new JPanel();
        //statPanelLinks.setLayout(new FlowLayout(FlowLayout.LEFT));
        statPanelLinks.add(statPanelInputField, BorderLayout.NORTH);
        statPanelInputField.add(new JLabel("Name:   "));       
        statPanelInputField.add(statNameTextField);
        
        JPanel statPanelObenButtonField = new JPanel();
        statPanelObenButtonField.setLayout(new FlowLayout(FlowLayout.LEFT));
        statPanelLinks.add(statPanelObenButtonField, BorderLayout.CENTER);
        statPanelObenButtonField.add(modifyStatButton);
        
        JTextArea hintStatus = new JTextArea("Hint: Top entry corresponds \n"
                + "to the default setting!", 2,1);
        hintStatus.setBackground(null);
        hintStatus.setFont(new Font(hintStatus.getFont().getFontName(), 1, 13));
        hintStatus.setLineWrap(true);
        statPanelLinks.add(hintStatus, BorderLayout.SOUTH);
        
        
        //Status rechte Seite (Liste)
        JPanel statPanelRechts = new JPanel();
        statPanelRechts.setLayout(new BorderLayout());
        statPanel.add(statPanelRechts);
        
        statList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane statScrollPane = new JScrollPane(statList);
        statPanelRechts.add(statScrollPane, BorderLayout.CENTER);     
        
        catPanel.add(statPanel, BorderLayout.CENTER);
    }
        
    
    private void initMainFrame() {
        mainFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        mainFrame.setLayout(new BorderLayout());
        mainFrame.add(centerPane, BorderLayout.CENTER); 
        
        try {
            //System.out.println(getClass().getResource("icon.png"));
            Image image = ImageIO.read(getClass().getResource("icon.png"));
            mainFrame.setIconImage(image);
        } catch (IOException e) {
            System.out.println("Can't find logo.");
            e.printStackTrace();
        } 
        
        centerPane.addTab("Projects", projectPanel);
        centerPane.addTab("Categories/Status", catPanel);
        
        mainFrame.setSize(500,  500);     
        mainFrame.setLocationRelativeTo(null);        
    }   
    
    public void cleanFields() {
        projectTextField.setText("");
        orderNrTextField.setText("");
        subOrderNrTextField.setText("");
        categoryComboBox.setSelectedIndex(-1);
    }
    
    //Getter
    public JFrame getMainFrame() {return mainFrame;}
    public JTextField getSearchProjectTextField() {return searchProjectTextField;}
    
    public JTextField getOrderNrTextField() {return orderNrTextField;}
    public JTextField getProjectTextField() {return projectTextField;}
    public JComboBox getCategoryComboBox() {return categoryComboBox;}
    public JCheckBox getOrderVisible() {return orderVisible;}
    public JButton getNewButton() {return newButton;}
    public JButton getModifyButton() {return modifyButton;}
    public JButton getDeleteButton() {return deleteButton;}  
    
    public JTextField getSubOrderNrTextField() {return subOrderNrTextField;}
    public JTextField getSubProjectTextField() {return subProjectTextField;}
    public JCheckBox getSubOrderVisible() {return subOrderVisible;}
    public JButton getNewSubButton() {return newSubButton;}
    public JButton getModifySubButton() {return modifySubButton;}
    public JButton getDeleteSubButton() {return deleteSubButton;}  
    
    public JCheckBox getVisibleBox() {return visibleBox;}
    public JList getOrderList() {return orderList;}
    public JList getSubOrderList() {return subOrderList;}
    public DefaultListModel<String> getListModelOrder() {return listModelOrder;}
    public DefaultListModel<String> getListModelSubOrder() {return listModelSuborder;}
    
    //Category Panel
    public JTextField getCatNameTextField() {return catNameTextField;}
    public JButton getNewCatButton() {return newCatButton;}
    public JButton getModifyCatButton() {return modifyCatButton;}
    public JButton getDeleteCatButton() {return deleteCatButton;} 
    public JList getCatList() {return catList;}
    public DefaultListModel<String> getListModelCat() {return listModelCat;}
  
    //Status Panel
    public JTextField getStatNameTextField() {return statNameTextField;}
    public JButton getModifyStatButton() {return modifyStatButton;}
    public DefaultListModel<String> getListModelStat() {return listModelStat;}
    public JList getStatList() {return statList;}
    
}
