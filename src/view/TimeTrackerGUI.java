package view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.table.*;

//import org.jdatepicker.impl.*; --> ersetzt durch Maven-Dependency
import net.sourceforge.jdatepicker.impl.*;

import controller.ControllerProjectAdministration;
import persistence.DBConnector;
import util.DigitalClock;
import util.SteppedComboBox;

public class TimeTrackerGUI {
    private JFrame mainFrame = new JFrame("ProjectTimeTracker V1.3");
    SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
    
    //Upper Panel
    private JPanel upperPanel = new JPanel();
    private JDatePickerImpl datePicker;
    private JButton startButton = new JButton("START");
    private JButton stopButton = new JButton("STOP / Pause");
    private JButton projectsButton = new JButton("Projects");
    private JButton overviewButton = new JButton("Overview");
    
    //Record Panel
    private JPanel recordPanel = new JPanel(new BorderLayout());
    private JTextField startTime = new JTextField("", 3);
    private JTextField stopTime = new JTextField("", 3);
    private JTextField pauseTime = new JTextField("",2);
    private JTextField searchField = new JTextField("", 9);
    //private JComboBox<String> orderComboBox = new JComboBox<String>();
    private SteppedComboBox orderComboBox = new SteppedComboBox();
    private JTextField catLabel = new JTextField("", 7);
    private JButton saveButton = new JButton("save");
    private SteppedComboBox subOrderComboBox = new SteppedComboBox();
    private SteppedComboBox statusComboBox = new SteppedComboBox();
    private JTextField commentTextField = new JTextField("", 10);

    //Overview Panel
    private JPanel overviewPanel = new JPanel(new BorderLayout());
    private JTable overviewTable = new JTable();   
    private JScrollPane scrollPane = new JScrollPane(overviewTable);
    private JButton modifyButton = new JButton("modify");
    private JButton deleteButton = new JButton("delete");
    private JButton cancelButton = new JButton("cancel");
    private JButton closeButton = new JButton("EXIT");
    
    public TimeTrackerGUI() {
        initUpperPanel();
        initRecordPanel();
        initOverviewPanel();
        initMainFrame();
        mainFrame.setVisible(true);
        
    }
    
    private void initUpperPanel() {
        upperPanel.setLayout(new BorderLayout());       
        
        //Panel für Datum + Zeit
        JPanel upperWestPanel = new JPanel(new GridLayout(2,0,5,5));
        upperWestPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        
        //DatumAuswahlBox
        UtilDateModel model = new UtilDateModel();
        Calendar rightNow = Calendar.getInstance();
        model.setDate(rightNow.get(Calendar.YEAR), rightNow.get(Calendar.MONTH), rightNow.get(Calendar.DAY_OF_MONTH));
        model.setSelected(true);
        
        /*Properties p = new Properties();
        p.put("text.today", "Today");
        p.put("text.month", "Month");
        p.put("text.year", "Year");
        --> Properties für JDatePicker wird erst ab V1.3.4 benötigt, via Maven aber nur V1.3.2 verfügbar*/
        
        JDatePanelImpl datePanel = new JDatePanelImpl(model);
        datePicker = new JDatePickerImpl(datePanel, new DateComponentFormatter());
        datePicker.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Date"),
                BorderFactory.createBevelBorder(BevelBorder.LOWERED)));
        JFormattedTextField dateTextField = datePicker.getJFormattedTextField();
        dateTextField.setFont(new Font("Monospaced", 1, 20));
        dateTextField.setBackground(Color.WHITE);
        dateTextField.setHorizontalAlignment(JTextField.CENTER);
        upperWestPanel.add(datePicker);
        
        //Uhrzeit
        JLabel clockLabel = new DigitalClock().getDigitalClock();
        clockLabel.setFont(new Font("Monospaced", 1, 20));
        clockLabel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Time"),
                BorderFactory.createBevelBorder(BevelBorder.LOWERED)));
        //clockLabel.setBackground(Color.WHITE);
        clockLabel.setOpaque(true);
        clockLabel.setHorizontalAlignment(JLabel.CENTER);
        upperWestPanel.add(clockLabel);

        upperPanel.add(upperWestPanel, BorderLayout.WEST);
        
        //Buttons START + STOPP
        JPanel upperCenterPanel = new JPanel(new GridLayout(2,0,5,5));
        upperCenterPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(5, 0, 5, 100), 
                BorderFactory.createTitledBorder("Record")));
        startButton.setBackground(Color.GREEN);
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (stopTime.getText().equals("")) {
                    startTime.setText(clockLabel.getText());
                    stopButton.setBackground(Color.RED);
                    startButton.setBackground(Color.LIGHT_GRAY);
                }
                else {
                    try {
                        Date restartDate = timeFormatter.parse(clockLabel.getText());
                        Date stopDate = timeFormatter.parse(stopTime.getText());
                        long pauseLong = restartDate.getTime() - stopDate.getTime() 
                                + ((pauseTime.getText().equals("")) ? 0 : Long.parseLong(pauseTime.getText()));
                        long pauseMinutes = pauseLong/(1000*60);
                        pauseTime.setText(String.valueOf(pauseMinutes));
                        stopTime.setText("");
                        stopButton.setBackground(Color.RED);
                        startButton.setBackground(Color.LIGHT_GRAY);
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }           
                }; 
            }          
        });
        stopButton.setBackground(Color.GRAY);
        stopButton.setForeground(Color.WHITE);
        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stopTime.setText(clockLabel.getText());   
                stopButton.setBackground(Color.GRAY);
                startButton.setBackground(Color.GREEN);
            }          
        });
        upperCenterPanel.add(startButton);
        upperCenterPanel.add(stopButton);
        upperPanel.add(upperCenterPanel, BorderLayout.CENTER);
        
        //Buttons rechts
        JPanel upperEastPanel = new JPanel(new GridLayout(2,0,5,5));
        upperEastPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(5, 5, 5, 100), 
                BorderFactory.createTitledBorder("Tools")));
        upperEastPanel.add(projectsButton);
        upperEastPanel.add(overviewButton);
        upperPanel.add(upperEastPanel, BorderLayout.EAST);
    }
    
    private void initRecordPanel() {
        
        //1. Zeit-Felder
        JPanel recordLeftPanel = new JPanel(new GridLayout(2,0,5,5));
        recordLeftPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(5, 5, 5, 5),
                BorderFactory.createTitledBorder("Time")));
        
        JPanel recordLeftNorthPanel = new JPanel(new GridLayout(0,2));
        JPanel recordLeftSouthPanel = new JPanel(new GridLayout(0,2));
        
        recordLeftNorthPanel.add(new JLabel("Start: "));
        recordLeftNorthPanel.add(startTime);
        recordLeftPanel.add(recordLeftNorthPanel);
        
        recordLeftSouthPanel.add(new JLabel("Stop: "));
        recordLeftSouthPanel.add(stopTime);
        recordLeftPanel.add(recordLeftSouthPanel);
        
        recordPanel.add(recordLeftPanel, BorderLayout.WEST);
        
        //2. Project Felder
        JPanel recordCenterPanel = new JPanel(new BorderLayout());
        
        JPanel recordCenterLeftPanel = new JPanel(new GridLayout(2,0,5,5));
        recordCenterLeftPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(5, 5, 5, 5),
                BorderFactory.createTitledBorder("Project")));
        
        
        JPanel recordCenterLeftObenPanel = new JPanel(new BorderLayout());
        recordCenterLeftObenPanel.add(new JLabel("Search: "), BorderLayout.WEST);
        recordCenterLeftObenPanel.add(searchField, BorderLayout.CENTER);
        recordCenterLeftPanel.add(recordCenterLeftObenPanel);
        
        JPanel recordCenterLeftUntenPanel = new JPanel(new BorderLayout());
        orderComboBox.setPopupWidth(300);
        orderComboBox.setPreferredSize(new Dimension(150, 1));
        recordCenterLeftUntenPanel.add(orderComboBox, BorderLayout.WEST);
        recordCenterLeftUntenPanel.add(new JLabel(""), BorderLayout.CENTER);
        recordCenterLeftPanel.add(recordCenterLeftUntenPanel);
        
        recordCenterPanel.add(recordCenterLeftPanel, BorderLayout.WEST);
        recordPanel.add(recordCenterPanel, BorderLayout.CENTER);      
        
        
        //3. Save Button
        JPanel recordCenterWestPanel = new JPanel(new BorderLayout());
        recordCenterWestPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        recordCenterWestPanel.add(saveButton, BorderLayout.CENTER);
        recordCenterPanel.add(recordCenterWestPanel, BorderLayout.CENTER);
        
        //3.1 Pause Field
        JPanel recordCenterEastPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        //recordCenterEastPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        recordCenterEastPanel.add(new JLabel("Pause: "));
        recordCenterEastPanel.add(pauseTime);
        recordCenterEastPanel.add(new JLabel("min"));
        recordCenterWestPanel.add(recordCenterEastPanel, BorderLayout.SOUTH);
        
        //4. Additional Fields
        JPanel recordRightPanel = new JPanel(new BorderLayout());
        recordRightPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(5, 5, 5, 5),
                BorderFactory.createTitledBorder("Additonal")));
        
        JPanel recordRightWestPanel = new JPanel(new GridLayout(2,0,5,5));
        
        JPanel recordRightWestObenPanel = new JPanel(new BorderLayout());
        recordRightWestObenPanel.add(new JLabel("Sub/Status: "), BorderLayout.CENTER);
        subOrderComboBox.setPreferredSize(new Dimension(100,1));
        subOrderComboBox.setPopupWidth(250);
        recordRightWestObenPanel.add(subOrderComboBox, BorderLayout.EAST);
        recordRightWestPanel.add(recordRightWestObenPanel);
        
        JPanel recordRightWestUntenPanel = new JPanel(new BorderLayout());
        recordRightWestUntenPanel.add(new JLabel("Comment: "), BorderLayout.WEST);
        recordRightWestUntenPanel.add(commentTextField, BorderLayout.CENTER);
        recordRightWestPanel.add(recordRightWestUntenPanel);
        recordRightPanel.add(recordRightWestPanel, BorderLayout.WEST);
        
        JPanel recordRightCenterPanel = new JPanel(new GridLayout(2,0,5,5));
        recordRightCenterPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));
        statusComboBox.setPreferredSize(new Dimension(70,1));
        statusComboBox.setPopupWidth(120);
        recordRightCenterPanel.add(statusComboBox);
        catLabel.setEditable(false);
        recordRightCenterPanel.add(catLabel);
        recordRightPanel.add(recordRightCenterPanel, BorderLayout.CENTER);
        
        //5. Delete Button
        /*JPanel recordRigthEastPanel = new JPanel(new BorderLayout());
        recordRigthEastPanel.setBorder(BorderFactory.createEmptyBorder(5, 20, 5, 5));
        recordRigthEastPanel.add(deleteButton, BorderLayout.CENTER);
        recordRightPanel.add(recordRigthEastPanel, BorderLayout.EAST);*/
        
        recordPanel.add(recordRightPanel, BorderLayout.EAST);
        
        recordPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(5, 5, 5, 5), 
                BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(255,127,80), 2), "Record")));
        upperPanel.add(recordPanel, BorderLayout.SOUTH);
    }
    
    private void initOverviewPanel() {
        //Tabelle 'Overview'
        scrollPane.setLayout(new ScrollPaneLayout());
        overviewPanel.add(scrollPane, BorderLayout.CENTER);
        overviewPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(10, 5, 5, 5), 
                BorderFactory.createTitledBorder("Overview")));
        
        //Mache nur Spalte 5 bearbeitbar
        DefaultTableModel overviewTableModel = new DefaultTableModel()/* {
            @Override
            public boolean isCellEditable(int row, int column) {
                if (column == 5) return true;
                return false;
            }
        }*/;
        
        DefaultTableColumnModel overviewTableColumnModel = new DefaultTableColumnModel();
        overviewTable.setModel(overviewTableModel);
        overviewTable.setColumnModel(overviewTableColumnModel);
        
        String[] labels = {"ID", "Start", "Stop", "Pause", "\u03a3 [min]", "OrderNr", "Project", "Category", "SubOrder", "SubProject", "Status", "Comment"};
        overviewTableModel.setColumnIdentifiers(labels);
        //overviewTableColumnModel.removeColumn(overviewTableColumnModel.getColumn(0)); 
        
        //Spaltenbreiten Einstellen
        TableColumn column = null;
        DefaultTableCellRenderer tableRenderer = new DefaultTableCellRenderer();
        tableRenderer.setHorizontalAlignment( JLabel.CENTER );
        for (int i = 0; i < labels.length; i++) {
            column = overviewTable.getColumnModel().getColumn(i);
            column.setCellRenderer( tableRenderer );
            if (i == 0) {
                column.setPreferredWidth(1);
            } else if (i > 0 && i < 5) {
                column.setPreferredWidth(50);
            } else {
                column.setPreferredWidth(100);
            }
        }
        
        //SouthPanel mit Buttons
        JPanel overviewSouthPanel = new JPanel(new GridLayout(0, 8 , 5, 5));
        overviewPanel.add(overviewSouthPanel, BorderLayout.SOUTH);
        overviewSouthPanel.add(modifyButton);
        modifyButton.setVisible(false);
        overviewSouthPanel.add(deleteButton);
        deleteButton.setVisible(false);
        overviewSouthPanel.add(cancelButton);
        cancelButton.setVisible(false);
        overviewSouthPanel.add(new JLabel(""));
        overviewSouthPanel.add(new JLabel(""));
        overviewSouthPanel.add(new JLabel(""));
        overviewSouthPanel.add(new JLabel(""));
        overviewSouthPanel.add(closeButton);     
    }
    
    private void initMainFrame() {
        mainFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        mainFrame.setLayout(new BorderLayout());
        try {
            //System.out.println(getClass().getResource("icon.png"));
            Image image = ImageIO.read(getClass().getResource("icon.png"));
            mainFrame.setIconImage(image);
        } catch (IOException e) {
            System.out.println("Can't find logo.");
            e.printStackTrace();
        }     
        
        mainFrame.add(upperPanel, BorderLayout.NORTH);
        mainFrame.add(overviewPanel, BorderLayout.CENTER);
        
        mainFrame.setSize(730,  500);     
        mainFrame.setLocationRelativeTo(null);   
    }
    
    public void setBorderLabel(String label) {
        overviewPanel.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(10, 5, 5, 5), 
                BorderFactory.createTitledBorder(label)));
    }    
    
    //-------------------------------------------
    //Getter
    //-------------------------------------------
    
    public JFrame getMainFrame() {return mainFrame;}
    public JButton getCloseButton() {return closeButton;}

    //Upper Panel
    public JPanel getUpperPanel() {return upperPanel;}
    public JDatePickerImpl getDatePicker() {return datePicker;}
    public JButton getStartButton() {return startButton;}
    public JButton getStopButton() {return stopButton;}
    public JButton getProjectsButton() {return projectsButton;}
    public JButton getOverviewButton() {return overviewButton;}
    
    //Record Panel
    public JPanel getRecordPanel() {return recordPanel;}
    public JTextField getStartTime() {return startTime;}
    public JTextField getStopTime() {return stopTime;}
    public JTextField getPauseTime() {return pauseTime;}
    public JTextField getSearchField() {return searchField;}
    public JComboBox getOrderComboBox() {return orderComboBox;}
    public JTextField getCatLabel() {return catLabel;}
    public JButton getSaveButton() {return saveButton;}
    public JComboBox getSubOrderComboBox() {return subOrderComboBox;}
    public JComboBox getStatusComboBox() {return statusComboBox;}
    public JTextField getCommentTextField() {return commentTextField;}

    //Overview Panel
    public JPanel getOverviewPanel() {return overviewPanel;}
    public JScrollPane getScrollPane() {return scrollPane;}
    public JTable getOverviewTable() {return overviewTable;}
    public JButton getModifyButton() {return modifyButton;}
    public JButton getDeleteButton() {return deleteButton;}
    public JButton getCancelButton() {return cancelButton;}
}
