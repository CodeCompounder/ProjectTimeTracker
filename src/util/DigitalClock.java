package util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.Timer;

public class DigitalClock {
    final DateFormat theClockFormat = new SimpleDateFormat("HH:mm");
    final DateFormat theClockFormat2 = new SimpleDateFormat("HH mm");
    final JLabel theClockLabel = new JLabel();
    
    public DigitalClock() {
        /* a timer with delay of one second */
        final Timer clockTimer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                String text = theClockFormat.format(new Date());
                /* synchronized since threaded access */
                synchronized(theClockLabel.getTreeLock()) {
                    theClockLabel.setText(text);
                }
            }
        });
        clockTimer.start();
    }
    
    public JLabel getDigitalClock() {
        return theClockLabel;
    }
}
