package util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeValidator {

    public boolean isTimeValid(String time) {
        
        if (time == null) return false;
        if (time.length() != 5) return false;
        
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        sdf.setLenient(false);
        
        try {
            //if not valid, it will throw ParseException
             Date checkedTime = sdf.parse(time);
             //System.out.println(checkedTime);
         } catch (ParseException e) {
             //e.printStackTrace();
             return false;
         }

         return true;
    }
}
