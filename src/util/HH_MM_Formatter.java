package util;

public class HH_MM_Formatter {

    public static String formatHourMinute(int minutes) {
        String h = String.format("%02d", minutes/60);
        String m = String.format("%02d", minutes%60);                      
        return (h + ":" + m);
    }
    
    public static String formatHourMinute(String minutes) throws NumberFormatException{
        int minutesInt = Integer.parseInt(minutes);
        return formatHourMinute(minutesInt);
    }
}
