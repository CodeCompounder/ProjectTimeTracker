package main;

import javax.swing.SwingUtilities;
import controller.ControllerTimeTrackerGUI;
import persistence.DBConnector;

public class Main {

    public static void main(String[] args) {
        
        DBConnector db = new DBConnector();
        
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ControllerTimeTrackerGUI(db);
            }
        });
    }
}
