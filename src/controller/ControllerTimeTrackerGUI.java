package controller;

import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.text.DateFormat;
import java.util.*;
import java.util.Date;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.*;

import persistence.DBConnector;
import util.*;
import view.*;

public class ControllerTimeTrackerGUI {
    private TimeTrackerGUI gui;
    private DBConnector dbConnector;
    private ArrayList<Integer> orderIDBuffer = new ArrayList<Integer>();
    private ArrayList<Integer> orderIDBufferTable = new ArrayList<Integer>();
    private ArrayList<Integer> subOrderIDBuffer = new ArrayList<Integer>();
    private ArrayList<Integer> subOrderIDBufferTable = new ArrayList<Integer>();
    private boolean itemEventTrigger = true;
    private SteppedComboBox editOrderBox = new SteppedComboBox();
    private SteppedComboBox editSubOrderBox = new SteppedComboBox();
    private TableMouseAdapter tableMouseListener = new TableMouseAdapter();
    private CellEditorListener editOrderBoxListener = new EditOrderBoxListener();
    private TimeValidator timeValidator = new TimeValidator();
    
    public ControllerTimeTrackerGUI(DBConnector dbConnector) {
        this.gui = new TimeTrackerGUI();
        this.dbConnector = dbConnector;
        initController();
        fillOverviewTable();
    }
    
    private void initController() {
        
        //Search Feld initialisieren
        fillOrderComboBox();
        gui.getSearchField().addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                fillOrderComboBox();
                gui.getOrderComboBox().setSelectedIndex(-1);
                if (gui.getOrderComboBox().getItemCount() > 0) {
                    gui.getOrderComboBox().setSelectedIndex(0);
                }              
            }
        });
        
        //Projects Button
        gui.getProjectsButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProjectAdministration pa = new ProjectAdministration();
                new ControllerProjectAdministration(pa, dbConnector);
                pa.getMainFrame().addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosed(WindowEvent f) {
                        gui.getSubOrderComboBox().setSelectedIndex(-1);
                        fillOrderComboBox();                       
                    }               
                });
            }          
        });
        
        //Overview Button
        gui.getOverviewButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TimeOverview ta = new TimeOverview();
                new ControllerTimeOverview(ta, dbConnector);
                ta.getMainFrame().addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosed(WindowEvent f) {
                        gui.getSubOrderComboBox().setSelectedIndex(-1);
                        fillOrderComboBox();                       
                    }               
                });
            }          
        });
        
        //OrderComboBox Listener (Klasse ComboBoxListener() siehe unten)
        gui.getOrderComboBox().addItemListener(new ComboBoxListener());
        
        //Status Combo Box initialisieren
        ResultSet rsStatus = dbConnector.selectStatus();
        try {
            while (rsStatus.next()) {
                String status  = rsStatus.getString("stat_name");
                gui.getStatusComboBox().addItem(status);
            }
        } catch (SQLException f) {
            f.printStackTrace();
        }     
        
        //Save Button
        gui.getSaveButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Date date = (Date) gui.getDatePicker().getModel().getValue();
                Timestamp dateStamp =  new java.sql.Timestamp(date.getTime());
                Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Paris"));
                cal.setTime(date);
                if (cal.get(Calendar.YEAR) < 2000) {
                    String message = "Choose Year after 2000.";
                    JOptionPane.showMessageDialog(gui.getMainFrame(), message, "Invalid year", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
                
                //Meldung, falls kein Project ausgewählt
                if (gui.getOrderComboBox().getSelectedIndex() == -1) {
                    String message = "Select valid project!";
                    JOptionPane.showMessageDialog(gui.getMainFrame(), message, "Invalid Project", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
                String startTime = gui.getStartTime().getText();
                String stopTime = gui.getStopTime().getText();
                int pauseTime;
                try {
                    pauseTime = Integer.parseInt((gui.getPauseTime().getText().equals(""))? "0" : gui.getPauseTime().getText());
                } catch (NumberFormatException f) {
                    String message = "Invalid input in Pause field. Use format mm.";
                    JOptionPane.showMessageDialog(gui.getMainFrame(), message, "Error", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
                int orderID = orderIDBuffer.get(gui.getOrderComboBox().getSelectedIndex());
                if (!(timeValidator.isTimeValid(startTime))|| !(timeValidator.isTimeValid(stopTime))) {
                    String message = "Invalid input in Start/Stop fields. Use format hh:mm.";
                    JOptionPane.showMessageDialog(gui.getMainFrame(), message, "Error", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
                Integer subOrderID = (gui.getSubOrderComboBox().getSelectedIndex() == -1) ? null : (subOrderIDBuffer.get(gui.getSubOrderComboBox().getSelectedIndex()));
                String comment = gui.getCommentTextField().getText();
                int statusID = (gui.getStatusComboBox().getSelectedIndex())+1;          
                dbConnector.addEffort(dateStamp, startTime, stopTime, pauseTime, orderID, subOrderID, comment, statusID);
                fillOverviewTable();
                gui.getStartTime().setText(gui.getStopTime().getText());
                gui.getStopTime().setText("");
                gui.getCommentTextField().setText("");
            }
        });
        
        //Exit bzw. Close Button
        gui.getCloseButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dbConnector.closeDB();
                gui.getMainFrame().dispose();
            }            
        });
        
        //MainFrame schließen -> Disconnect DB
        gui.getMainFrame().addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                dbConnector.closeDB();                
            }            
        });
        
        //Date Picker
        gui.getDatePicker().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fillOverviewTable(); 
                //Färbe DatePicker gelb, falls Datum nicht heute
                GregorianCalendar now = new GregorianCalendar();
                Date date = (Date) gui.getDatePicker().getModel().getValue();
                DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT);
                if (String.valueOf(df.format(date)).equals(String.valueOf(df.format(now.getTime())))) {
                    gui.getDatePicker().getJFormattedTextField().setBackground(Color.WHITE);
                } else {
                    gui.getDatePicker().getJFormattedTextField().setBackground(Color.YELLOW);
                }
            }            
        }); 
        
        //Overview-Table add MouseListener (siehe unten)
        gui.getOverviewTable().addMouseListener(tableMouseListener);
        
        //Cancel Button
        gui.getCancelButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fillOverviewTable(); 
                resetOverviewTable();
            }            
        });
        
        //Delete Button
        gui.getDeleteButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String effortID = (String) gui.getOverviewTable().getValueAt(0, 0);
                //System.out.println(effortID);
                int i = JOptionPane.showConfirmDialog(gui.getMainFrame(),
                        "Delete selected effort?",
                        "Delete", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                if (i == 1) return;
                dbConnector.deleteEffort(effortID);
                fillOverviewTable();
                resetOverviewTable();
            }            
        });
        
        gui.getModifyButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //System.out.println(gui.getOverviewTable().getValueAt(0, 4));
                gui.getOverviewTable().getDefaultEditor(getClass()).stopCellEditing();
                int effortID = Integer.valueOf((String) gui.getOverviewTable().getValueAt(0, 0));
                String startTime = (String) gui.getOverviewTable().getValueAt(0, 1);
                String stopTime = (String) gui.getOverviewTable().getValueAt(0, 2);
                int pauseTime;
                try {
                    pauseTime = Integer.valueOf((String) gui.getOverviewTable().getValueAt(0, 3));
                } catch (NumberFormatException f) {
                    String message = "Invalid input in Pause field. Use format mm.";
                    JOptionPane.showMessageDialog(gui.getMainFrame(), message, "Error", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
                if (!(timeValidator.isTimeValid(startTime))|| !(timeValidator.isTimeValid(stopTime))) {
                    String message = "Invalid input in Start/Stop fields. Use format hh:mm.";
                    JOptionPane.showMessageDialog(gui.getMainFrame(), message, "Error", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
                
                int orderID = orderIDBufferTable.get(editOrderBox.getSelectedIndex());
                Integer subOrderID = (editSubOrderBox.getSelectedIndex() == -1) ? null : subOrderIDBufferTable.get(editSubOrderBox.getSelectedIndex());
                String comment = (String) gui.getOverviewTable().getValueAt(0, 6);         
                dbConnector.modifyEffort(effortID, startTime, stopTime, pauseTime, orderID, subOrderID, comment);
                fillOverviewTable();
                resetOverviewTable();
            }            
        });
    }

    
    private void fillOrderComboBox() {
        itemEventTrigger = false;  //Flag setzen, damit kein ItemEvent der ComboBox ausgelöst wird.
        gui.getOrderComboBox().removeAllItems();
        orderIDBuffer.clear();
        ResultSet rsOrders = dbConnector.selectOrders(gui.getSearchField().getText(), true);
        try {
            while (rsOrders.next()) {
                int id = rsOrders.getInt("main_id");
                String project = rsOrders.getString("main_desc");
                int orderNr  = rsOrders.getInt("main_nr");
                //System.out.println("add "+ id + " to orderIDBuffer..." + orderIDBuffer.size());
                orderIDBuffer.add(id);
                gui.getOrderComboBox().addItem(orderNr + " | " + project);
            }
        } catch (SQLException e) {
            System.out.println("Read ResultSet failed.");
            e.printStackTrace();
        }
        itemEventTrigger = true;
        gui.getOrderComboBox().setSelectedIndex(-1);
    }
    
    private void fillOverviewTable() {
        
        //Fülle Tabelle (nach Öffnen, Speichern und Datumsänderung):
        DefaultTableModel overviewTableModel = (DefaultTableModel) gui.getOverviewTable().getModel();
        overviewTableModel.setRowCount(0);
        //gui.getOverviewTable().removeAll();
        Date date = (Date) gui.getDatePicker().getModel().getValue();
        Timestamp dateStamp =  new java.sql.Timestamp(date.getTime());
        ResultSet rsEfforts = dbConnector.selectEfforts(dateStamp);
        int columnCount = gui.getOverviewTable().getModel().getColumnCount();
        String[] data = new String[columnCount];
        try {
            while (rsEfforts.next()) {
                for (int i = 1 ; i <= columnCount ; i++) {
                    if (i == 5) {
                        data[i-1] = HH_MM_Formatter.formatHourMinute(rsEfforts.getString(i));
                    } else {
                        data[i-1] = rsEfforts.getString(i);
                    }
                }
                overviewTableModel.addRow(data);
            }    
            
            //GesamtStunden anzeigen
            ResultSet rsTotalTime = dbConnector.selectTotalTime(dateStamp);
            gui.setBorderLabel("Overview | Total time " + HH_MM_Formatter.formatHourMinute(rsTotalTime.getInt(1)));
            
        } catch (SQLException e) {
            e.printStackTrace();
        }   
    }
    
    private class EditOrderBoxListener implements CellEditorListener {

        @Override
        public void editingStopped(ChangeEvent e) {
            editSubOrderBox.removeAllItems();
            int index = editOrderBox.getSelectedIndex();
            //System.out.println("index = " + index);
            if (index == -1) return;
            int id = orderIDBufferTable.get(index);
            //System.out.println("id = " + id);
            subOrderIDBufferTable.clear();
            ResultSet rsSubOrdersTable = dbConnector.selectSubOrders(true, id);
            System.out.println("Init ComboBox SubOrders...");
            try {
                while (rsSubOrdersTable.next()) {
                    int subID = rsSubOrdersTable.getInt("sub_id");
                    String subProject = rsSubOrdersTable.getString("sub_desc");
                    String subNr  = rsSubOrdersTable.getString("sub_nr");
                    subOrderIDBufferTable.add(subID);
                    //System.out.println("subID = "+ subID + " / bufferIndex = " + subOrderIDBuffer.size());
                    editSubOrderBox.addItem(subNr + " | " + subProject);
                }
            } catch (SQLException f) {
                f.printStackTrace();
            }     
        }

        @Override
        public void editingCanceled(ChangeEvent e) {
            // TODO Auto-generated method stub
            
        }
        
    }
    
    private class ComboBoxListener implements ItemListener {

        @Override
        public void itemStateChanged(ItemEvent e) {
            if(e.getStateChange() == ItemEvent.SELECTED) {
                if (itemEventTrigger == false) {    //Falls Event von .addItem ausgelöst wird -> return
                    return;
                } else {
                    gui.getSubOrderComboBox().removeAllItems();
                    int index = gui.getOrderComboBox().getSelectedIndex();
                    //System.out.println("index = " + index);
                    if (index == -1) return;
                    int id = orderIDBuffer.get(index);
                    //System.out.println("id = " + id);
                    subOrderIDBuffer.clear();
                    ResultSet rsSubOrders = dbConnector.selectSubOrders(true, id);
                    try {
                        while (rsSubOrders.next()) {
                            int subID = rsSubOrders.getInt("sub_id");
                            String subProject = rsSubOrders.getString("sub_desc");
                            String subNr  = rsSubOrders.getString("sub_nr");
                            subOrderIDBuffer.add(subID);
                            //System.out.println("subID = "+ subID + " / bufferIndex = " + subOrderIDBuffer.size());
                            gui.getSubOrderComboBox().addItem(subNr + " | " + subProject);
                        }
                    } catch (SQLException f) {
                        f.printStackTrace();
                    }     
                    
                    ResultSet rsOrder = dbConnector.selectOrders(id);
                    try {
                        while (rsOrder.next()) {
                            String category  = rsOrder.getString("cat_name");
                            //System.out.println("subID = "+ subID + " / bufferIndex = " + subOrderIDBuffer.size());
                            gui.getCatLabel().setText(category);
                        }
                    } catch (SQLException f) {
                        f.printStackTrace();
                    }     
                }              
            }       
        }        
    }
    
    private class TableMouseAdapter extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent e) {
            
            gui.getOverviewTable().removeMouseListener(tableMouseListener);
            
            //1. Entferne alle nicht selektierten Zeilen und stelle Spaltenbreite ein
            int selectedRow = gui.getOverviewTable().getSelectedRow();
            int rowCount = gui.getOverviewTable().getModel().getRowCount();
            DefaultTableModel tableModel = (DefaultTableModel) gui.getOverviewTable().getModel();
            for (int i = rowCount-1 ; i >= 0 ; i--) {
                if (i != selectedRow) {
                    tableModel.removeRow(i);
                }
            }
            String[] removeColumns = {"\u03a3 [min]", "Project", "Category", "SubProject", "Status"};
            for (String s : removeColumns) {
                gui.getOverviewTable().removeColumn(gui.getOverviewTable().getColumn(s));
            }
            
            //Spaltenbreiten Einstellen
            TableColumn column = null;
            DefaultTableCellRenderer tableRenderer = new DefaultTableCellRenderer();
            tableRenderer.setHorizontalAlignment( JLabel.CENTER );
            for (int i = 0; i < gui.getOverviewTable().getColumnCount(); i++) {
                column = gui.getOverviewTable().getColumnModel().getColumn(i);
                column.setCellRenderer( tableRenderer );
                if (i == 0) {
                    column.setPreferredWidth(column.getPreferredWidth());
                } else {
                    column.setPreferredWidth(100);
                }
            }
            
            //2. Blende Buttons ein
            gui.getModifyButton().setVisible(true);
            gui.getDeleteButton().setVisible(true);
            gui.getCancelButton().setVisible(true);
            
            //3. Füge EditBoxen hinzu
            editOrderBox.removeAllItems();
            orderIDBufferTable.clear();
            ResultSet rsOrdersTable = dbConnector.selectOrders(true);
            System.out.println("Init ComboBox Orders...");
            try {
                while (rsOrdersTable.next()) {
                    int id = rsOrdersTable.getInt("main_id");
                    String project = rsOrdersTable.getString("main_desc");
                    int orderNr  = rsOrdersTable.getInt("main_nr");
                    //System.out.println("add "+ id + " to orderIDBuffer..." + orderIDBuffer.size());
                    orderIDBufferTable.add(id);
                    editOrderBox.addItem(orderNr + " | " + project);
                }
            } catch (SQLException f) {
                System.out.println("Read ResultSet failed.");
                f.printStackTrace();
            }                     
            
            editOrderBox.setPopupWidth(250);
            DefaultCellEditor orderEditor = new DefaultCellEditor(editOrderBox);
            gui.getOverviewTable().getColumnModel().getColumn(4).setCellEditor(orderEditor);
            
            editSubOrderBox.setPopupWidth(250);
            DefaultCellEditor subOrderEditor = new DefaultCellEditor(editSubOrderBox);
            gui.getOverviewTable().getColumnModel().getColumn(5).setCellEditor(subOrderEditor);
            
            orderEditor.addCellEditorListener(editOrderBoxListener);
            
            //4. Selektiere Order der angeklickten Zeile
            int orderNrCurrent = Integer.parseInt((String) gui.getOverviewTable().getValueAt(0, 4));
            ResultSet rsOrderID = dbConnector.selectOrderID(orderNrCurrent);
            int orderID = 0;
            try {
                while (rsOrderID.next()) {
                    orderID = rsOrderID.getInt("main_id");
                }
            } catch (SQLException f) {
                System.out.println("Read ResultSet failed.");
                f.printStackTrace();
            }
            int orderIndex = orderIDBufferTable.indexOf(orderID);            
            editOrderBox.setSelectedIndex(orderIndex);
            
            //5. Selektiere SubOrder der angeklickten Zeile
            String subOrderNrCurrent = (String) gui.getOverviewTable().getValueAt(0, 5);
            ResultSet rsSubOrderID = dbConnector.selectSubOrders(subOrderNrCurrent, orderID);
            int subOrderID = 0;
            try {
                while (rsSubOrderID.next()) {
                    subOrderID = rsSubOrderID.getInt("sub_id");
                }
            } catch (SQLException f) {
                System.out.println("Read ResultSet failed.");
                f.printStackTrace();
            }
            int subOrderIndex = subOrderIDBufferTable.indexOf(subOrderID);
            editSubOrderBox.setSelectedIndex(subOrderIndex);            
        }
    }    
    
    public void resetOverviewTable() {
        gui.getOverviewTable().getColumnModel().getColumn(4).getCellEditor().removeCellEditorListener(editOrderBoxListener);
        
        String[] labels = {"ef_id", "Start", "Stop", "Pause", "\u03a3 [min]", "OrderNr", "Project", "Category", "SubOrder", "SubProject", "Status", "Comment"};
        DefaultTableModel tableModel = (DefaultTableModel) gui.getOverviewTable().getModel();
        tableModel.setColumnIdentifiers(labels);
        
        gui.getModifyButton().setVisible(false);
        gui.getDeleteButton().setVisible(false);
        gui.getCancelButton().setVisible(false);
        
        gui.getOverviewTable().addMouseListener(tableMouseListener);
        
        //Spaltenbreiten Einstellen
        TableColumn column = null;
        DefaultTableCellRenderer tableRenderer = new DefaultTableCellRenderer();
        tableRenderer.setHorizontalAlignment( JLabel.CENTER );
        for (int i = 0; i < labels.length; i++) {
            column = gui.getOverviewTable().getColumnModel().getColumn(i);
            column.setCellRenderer( tableRenderer );
            if (i == 0) {
                column.setPreferredWidth(1);
            } else if (i > 0 && i < 5) {
                column.setPreferredWidth(50);
            } else {
                column.setPreferredWidth(100);
            }
        }
    }    
}
