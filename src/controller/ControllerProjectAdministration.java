package controller;

import persistence.DBConnector;
import view.ProjectAdministration;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.*;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.event.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ControllerProjectAdministration {
    private ProjectAdministration gui;
    private DBConnector dbConnector;
    
    private ArrayList<Integer> orderIDBuffer = new ArrayList<Integer>();
    private ArrayList<Integer> subOrderIDBuffer = new ArrayList<Integer>();    
    
    public ControllerProjectAdministration(ProjectAdministration gui, DBConnector dbConnector) {
        this.gui = gui;
        this.dbConnector = dbConnector;
        initProjectController();
        initCategoryController();
        initStatusController();
    }

    private void initProjectController() {
        
        //Orders initialisieren
        ResultSet rsOrders = dbConnector.selectOrders(gui.getVisibleBox().isSelected());
        fillOrderList(rsOrders);
        
        //Kategorien/Status initialisieren
        fillCatLists();
        fillStatList();
        
        //KeyListener für Eingabe in ProjectTextField (innere Klasse siehe unten)
        gui.getSearchProjectTextField().addKeyListener(new KeyAction());
        
        //ItemListener für VisibleBox
        gui.getVisibleBox().addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                Robot keyTrigger;
                try {
                    keyTrigger = new Robot();
                    gui.getSearchProjectTextField().requestFocus();
                    keyTrigger.keyRelease(17); //-> Löse KeyEvent vom Search-Feld aus! (17 = STRG-Taste)
                } catch (AWTException e1) {
                    e1.printStackTrace();
                }
                //Leere SubOrder-Liste (da keine Order-Auswahl mehr!)
                gui.getListModelSubOrder().removeAllElements();
                subOrderIDBuffer.clear();
            }            
        });
        
        //ActionListener für Neu-Button
        gui.getNewButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int orderNr = 0;
                try {
                    orderNr = Integer.valueOf(gui.getOrderNrTextField().getText());
                } catch (Exception ef) {
                    JOptionPane.showMessageDialog(gui.getMainFrame(), "Only numerical values for order no. allowed!", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }                      
                String description = gui.getProjectTextField().getText();
                String category = (String) gui.getCategoryComboBox().getSelectedItem();
                int visible = (gui.getVisibleBox().isSelected() ? 1 : 0);
                
                //Prüfen, ob Eintrag schon vorhanden
                String alreadyExistCheck = orderNr + " | " + description;
                if (gui.getListModelOrder().contains(alreadyExistCheck)) {
                    JOptionPane.showMessageDialog(gui.getMainFrame(), "Entry already exists!", "Error", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
                                
                dbConnector.addProject(description, orderNr, visible, category);
                gui.cleanFields();
                ResultSet rsOrdersNew = dbConnector.selectOrders(gui.getVisibleBox().isSelected());
                fillOrderList(rsOrdersNew);
            }            
        });
        
        //ActionListener für Modify Button
        gui.getModifyButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int index = gui.getOrderList().getSelectedIndex();
                if (index == -1) return;    //-1 entspricht keiner Auswahl -> Abbrechen
                int id = orderIDBuffer.get(index);
                int orderNr = Integer.valueOf(gui.getOrderNrTextField().getText());
                String description = gui.getProjectTextField().getText();
                String category = (String) gui.getCategoryComboBox().getSelectedItem();
                int visible = (gui.getOrderVisible().isSelected() ? 1 : 0);   //Wandle boolean in int für DB um
                dbConnector.updateOrder(id, description, orderNr, visible, category);
                    //Aktualisiere Listen-Ansicht
                ResultSet rsOrdersNew = dbConnector.selectOrders(gui.getVisibleBox().isSelected());
                fillOrderList(rsOrdersNew);
            }            
        });
        
        //ActionListener für Delete Button
        gui.getDeleteButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int index = gui.getOrderList().getSelectedIndex();
                if (index == -1) return;    //-1 entspricht keiner Auswahl -> Abbrechen
                int id = orderIDBuffer.get(index);
                String orderName = (String) gui.getOrderList().getSelectedValue();
                int i = JOptionPane.showConfirmDialog(gui.getMainFrame(),
                        "Delete Order '" + orderName + "' and associated Suborders?",
                        "Delete", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                if (i == 1) return;
                dbConnector.deleteProject(id);
                gui.cleanFields();
                ResultSet rsOrdersNew = dbConnector.selectOrders(gui.getVisibleBox().isSelected());
                fillOrderList(rsOrdersNew);
                gui.getListModelSubOrder().removeAllElements();
            }            
        });
        
        //ListSelectionListener für JList Orders
        gui.getOrderList().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(!e.getValueIsAdjusting()) return;    //Damit Block nur 1mal ausgeführt wird (ohne diese Zeile -> zweimal!)
                
                //1. Fülle Felder Nr, Kat.,... mit Daten aus DB
                int index = gui.getOrderList().getSelectedIndex();
                if (index == -1) return;
                int id = orderIDBuffer.get(index);
                //System.out.println("1. Index = " + index + " / ID = " + id);
                ResultSet rsSelectedOrder = dbConnector.selectOrders(id);
                try {
                    gui.getProjectTextField().setText(rsSelectedOrder.getString("main_desc"));
                    gui.getOrderNrTextField().setText(String.valueOf(rsSelectedOrder.getInt("main_nr")));
                    if (rsSelectedOrder.getInt("main_visible") == 1) {
                        gui.getOrderVisible().setSelected(true);
                    } else {
                        gui.getOrderVisible().setSelected(false);
                    }
                    gui.getCategoryComboBox().setSelectedItem(rsSelectedOrder.getString("cat_name"));                    
                    //System.out.println(rsSelectedOrder.getString("cat_name"));
                    
                    //2. Fülle Suborder-List aus
                    fillSubOrderList();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                    return;
                }              
            }            
        });    
        
        //Action-Listener für SubOrder-Buttons
        
        //1. Neu Sub Button   
        gui.getNewSubButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int orderIndex = gui.getOrderList().getSelectedIndex();
                if (orderIndex == -1) return;    //-1 entspricht keiner Auswahl -> Abbrechen
                int orderID = orderIDBuffer.get(orderIndex);
                String description = gui.getSubProjectTextField().getText();
                String subOrderNr = gui.getSubOrderNrTextField().getText();
                int visible = (gui.getSubOrderVisible().isSelected() ? 1 : 0);
                
                //Prüfen, ob Eintrag schon vorhanden
                String alreadyExistCheck = subOrderNr + " | " + description;
                if (gui.getListModelSubOrder().contains(alreadyExistCheck)) {
                    JOptionPane.showMessageDialog(gui.getMainFrame(), "Entry already exists!", "Error", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }

                dbConnector.addSubProject(orderID, subOrderNr, description, visible);
                fillSubOrderList();        
            }            
        });  
        
        //2. Modify Sub Button
        gui.getModifySubButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int subIndex = gui.getSubOrderList().getSelectedIndex();
                if (subIndex == -1) return;    //-1 entspricht keiner Auswahl -> Abbrechen
                int subID = subOrderIDBuffer.get(subIndex);
                
                String description = gui.getSubProjectTextField().getText();
                String subOrderNr = gui.getSubOrderNrTextField().getText();
                int visible = (gui.getSubOrderVisible().isSelected() ? 1 : 0);
                
                dbConnector.updateSubOrder(subID, subOrderNr, description, visible);
                    //Aktualisiere Listen-Ansicht
                fillSubOrderList();
            }            
        });
        
        //3. Delete Sub Button
        gui.getDeleteSubButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int index = gui.getSubOrderList().getSelectedIndex();
                if (index == -1) return;    //-1 entspricht keiner Auswahl -> Abbrechen
                int id = subOrderIDBuffer.get(index);
                String SubName = (String) gui.getSubOrderList().getSelectedValue();
                int i = JOptionPane.showConfirmDialog(gui.getMainFrame(),
                        "Delete SubOrder '" + SubName + "'?",
                        "Delete", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                if (i == 1) return;
                dbConnector.deleteSubProject(id);
                fillSubOrderList();
            }            
        });
        
        
        //ListSelectionListener für JList SubOrders
        gui.getSubOrderList().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(!e.getValueIsAdjusting()) return;    //Damit Block nur 1mal ausgeführt wird (ohne diese Zeile -> zweimal!)
                
                //1. Fülle Felder Nr, Name,... mit Daten aus DB
                int subIndex = gui.getSubOrderList().getSelectedIndex();
                if (subIndex == -1) return;
                int subID = subOrderIDBuffer.get(subIndex);
                //System.out.println("subIndex = " + subIndex + " / subID = "+subID);
                ResultSet rsSelectedSubOrder = dbConnector.selectSubOrders(subID);
                try {
                    gui.getSubProjectTextField().setText(rsSelectedSubOrder.getString("sub_desc"));
                    gui.getSubOrderNrTextField().setText(rsSelectedSubOrder.getString("sub_nr"));
                    if (rsSelectedSubOrder.getInt("sub_visible") == 1) {
                        gui.getSubOrderVisible().setSelected(true);
                    } else {
                        gui.getSubOrderVisible().setSelected(false);
                    }                    
                    //System.out.println(rsSelectedOrder.getString("cat_name"));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }            
        });    
    } 
    
    
    //--------------------------------------------------------------------
    //Initialisierung Kategorien
    //--------------------------------------------------------------------

    private void initCategoryController() {
        //fillCatBox(); --> Bereits bei initProjectController erledigt.
        
        //ListSelectionListener für JList Kategorien
        gui.getCatList().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(!e.getValueIsAdjusting()) return; 
                gui.getCatNameTextField().setText((String) gui.getCatList().getSelectedValue()); 
            }            
        });
        
      //ActionListener für Neu-Button
        gui.getNewCatButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String catName = gui.getCatNameTextField().getText();   
                if (gui.getListModelCat().contains(catName)) {
                    String message = "Category '" + catName + "' already exists.";
                    JOptionPane.showMessageDialog(gui.getMainFrame(), message, "New name required", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
                dbConnector.addCategory(catName);
                fillCatLists();
            }            
        });
        
      //ActionListener für Modify-Button
        gui.getModifyCatButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (gui.getCatList().isSelectionEmpty()) {
                    String message = "Select category from the list for modification.";
                    JOptionPane.showMessageDialog(gui.getMainFrame(), message, "No Selection", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
                String catNameOld = (String) gui.getCatList().getSelectedValue();  
                String catNameNew = gui.getCatNameTextField().getText();
                if (gui.getListModelCat().contains(catNameNew)) {
                    String message = "Category '" + catNameNew + "' already exists.";
                    JOptionPane.showMessageDialog(gui.getMainFrame(), message, "New name required", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
                dbConnector.modifyCategory(catNameOld, catNameNew);
                fillCatLists();
            }            
        });
        
        //ActionListener für Delete-Button
        gui.getDeleteCatButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String catName = (String) gui.getCatList().getSelectedValue();
                int i = JOptionPane.showConfirmDialog(gui.getMainFrame(),
                        "Delete category '" + catName + "'?",
                        "Delete", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                if (i == 1) return;
                dbConnector.deleteCategory(catName);
                fillCatLists();
            }            
        });
    }
    
    //--------------------------------------------------------------------
    //Initialisierung Status
    //--------------------------------------------------------------------
    private void initStatusController() {
        
        //ListSelectionListener für JList Status
        gui.getStatList().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(!e.getValueIsAdjusting()) return; 
                gui.getStatNameTextField().setText((String) gui.getStatList().getSelectedValue()); 
            }            
        });
        
        //ActionListener für Modify-Button
        gui.getModifyStatButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (gui.getStatList().isSelectionEmpty()) {
                    String message = "Select status from the list for modification.";
                    JOptionPane.showMessageDialog(gui.getMainFrame(), message, "No Selection", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
                String statNameOld = (String) gui.getStatList().getSelectedValue();  
                String statNameNew = gui.getStatNameTextField().getText();
                if (gui.getListModelStat().contains(statNameNew)) {
                    String message = "Status '" + statNameNew + "' already exists.";
                    JOptionPane.showMessageDialog(gui.getMainFrame(), message, "New name required", JOptionPane.INFORMATION_MESSAGE);
                    return;
                }
                dbConnector.modifyStatus(statNameOld, statNameNew);
                fillStatList();
            }            
        });
        
    }
    
    
    
    //--------------------------------------------------------------------
    //Hilfsmethoden
    //--------------------------------------------------------------------
    
    private void fillOrderList(ResultSet rs) {       
        gui.getListModelOrder().removeAllElements();
        orderIDBuffer.clear();
        try {
            while( rs.next() ) {
                int id = rs.getInt("main_id");
                String project = rs.getString("main_desc");
                int orderNr  = rs.getInt("main_nr");
                int visible = rs.getInt("main_visible");
                orderIDBuffer.add(id);
                if (visible == 1) {
                    gui.getListModelOrder().addElement(orderNr + " | " + project);
                    //System.out.println(orderNr + " | " + project);
                } else {
                    gui.getListModelOrder().addElement(orderNr + " | " + project + " | " + "NOT VISIBLE");
                    //System.out.println(orderNr + " | " + project + " | " + "not visible");
                }
            }
        } catch (SQLException e) {
            System.out.println("Read ResultSet failed.");
            e.printStackTrace();
            return;
        }
    }
    
    private void fillSubOrderList() {
        gui.getListModelSubOrder().removeAllElements();
        int index = gui.getOrderList().getSelectedIndex();
        if (index == -1) return;
        int id = orderIDBuffer.get(index);
        //System.out.println("2. Index = " + index + " / ID = " + id);
        subOrderIDBuffer.clear();
        ResultSet rsSubOrders = dbConnector.selectSubOrders(gui.getVisibleBox().isSelected(), id);
        try {
            while (rsSubOrders.next()) {
                int subID = rsSubOrders.getInt("sub_id");
                String subProject = rsSubOrders.getString("sub_desc");
                String subNr  = rsSubOrders.getString("sub_nr");
                int visible = rsSubOrders.getInt("sub_visible");
                subOrderIDBuffer.add(subID);
                //System.out.println("subID = "+ subID + " / bufferIndex = " + subOrderIDBuffer.size());
                if (visible == 1) {
                    gui.getListModelSubOrder().addElement(subNr + " | " + subProject);
                    //System.out.println(orderNr + " | " + project);
                } else {
                    gui.getListModelSubOrder().addElement(subNr + " | " + subProject + " | " + "NOT VISIBLE");
                    //System.out.println(orderNr + " | " + project + " | " + "not visible");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }        
    }
    
    private void fillCatLists() {
        gui.getCategoryComboBox().removeAllItems();
        gui.getListModelCat().removeAllElements();
        ResultSet rsCategories = dbConnector.selectCategories();
        try {
            while (rsCategories.next()) {
                String cat = rsCategories.getString("cat_name");
                gui.getCategoryComboBox().addItem(cat);
                gui.getListModelCat().addElement(cat);
            }
        } catch (SQLException e) {
            System.out.println("Read ResultSet Categories failed.");
            e.printStackTrace();
            return;
        }
        gui.getCategoryComboBox().setSelectedIndex(-1);
    }
    
    public void fillStatList() {
        gui.getListModelStat().removeAllElements();
        ResultSet rsStatus = dbConnector.selectStatus();
        try {
            while (rsStatus.next()) {
                String stat = rsStatus.getString("stat_name");
                gui.getListModelStat().addElement(stat);
            }
        } catch (SQLException e) {
            System.out.println("Read ResultSet Status failed.");
            e.printStackTrace();
            return;
        }
    }
    
    private class KeyAction implements KeyListener {

        @Override
        public void keyTyped(KeyEvent e) {
            // TODO Auto-generated method stub            
        }

        @Override
        public void keyPressed(KeyEvent e) {
            // TODO Auto-generated method stub            
        }

        @Override
        public void keyReleased(KeyEvent e) {
            gui.getListModelOrder().removeAllElements();
            //orderIDBuffer.clear();
            if (gui.getSearchProjectTextField().getText().equals("")) {
                ResultSet rsOrders = dbConnector.selectOrders(gui.getVisibleBox().isSelected());
                fillOrderList(rsOrders);
            } else {
                ResultSet rsOrdersInput;
                rsOrdersInput = dbConnector.selectOrders(gui.getSearchProjectTextField().getText(), gui.getVisibleBox().isSelected()); 
                fillOrderList(rsOrdersInput);   
            }            
        }        
    }
}
