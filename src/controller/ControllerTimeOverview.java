package controller;

import java.awt.event.*;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.table.DefaultTableModel;
import javax.swing.*;
import javax.swing.event.*;

import persistence.DBConnector;
import view.TimeOverview;
import util.HH_MM_Formatter;

public class ControllerTimeOverview {
    private TimeOverview gui;
    private DBConnector dbConnector;
    private JComboBox<String> statusBox = new JComboBox<String>();
    private Timestamp searchStartDate;
    private Timestamp searchEndDate;
    private StatusChangeTableModelListener tableListener = new StatusChangeTableModelListener();
    private ArrayList<String> statusBuffer = new ArrayList<String>();

    public ControllerTimeOverview(TimeOverview gui, DBConnector dbConnector) {
        this.gui = gui;
        this.dbConnector = dbConnector;
        initCategories();
        initStatus();
        initSearch();
        initUpperTable();
        initLowerTable();
    }

    private void initCategories() {
        DefaultListModel<String> catListModel = (DefaultListModel<String>) gui.getCatList().getModel();
        catListModel.removeAllElements();
        ResultSet rsCategories = dbConnector.selectCategories();
        System.out.println("Fill categories...");
        catListModel.addElement("*all*");
        try {
            while (rsCategories.next()) {
                String cat = rsCategories.getString("cat_name");
                catListModel.addElement(cat);
            }
        } catch (SQLException e) {
            System.out.println("Read ResultSet Categories failed.");
            e.printStackTrace();
            return;
        }
        gui.getCatList().setSelectedIndex(0);
    }
        

    private void initStatus() {
        DefaultListModel<String> statListModel = (DefaultListModel<String>) gui.getStatList().getModel();
        statListModel.removeAllElements();
        ResultSet rsStatus = dbConnector.selectStatus();
        System.out.println("Fill status...");
        statListModel.addElement("*all*");
        try {
            while (rsStatus.next()) {
                String cat = rsStatus.getString("stat_name");
                statListModel.addElement(cat);
            }
        } catch (SQLException e) {
            System.out.println("Read ResultSet Categories failed.");
            e.printStackTrace();
            return;
        }
        gui.getStatList().setSelectedIndex(0);        
    }

    private void initSearch() {
        gui.getSearchButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DefaultTableModel upperTableModel = (DefaultTableModel) gui.getUpperTable().getModel();
                upperTableModel.setRowCount(0);
                
                Date startDate = (Date) gui.getDatePickerStart().getModel().getValue();
                searchStartDate =  new java.sql.Timestamp(startDate.getTime());
                Date endDate = (Date) gui.getDatePickerEnd().getModel().getValue();
                searchEndDate =  new java.sql.Timestamp(endDate.getTime());
                String cat = gui.getCatList().getSelectedValue().equals("*all*") ? null : gui.getCatList().getSelectedValue();
                String stat = gui.getStatList().getSelectedValue().equals("*all*") ? null : gui.getStatList().getSelectedValue();
                ResultSet rsAllEfforts = dbConnector.selectAllEfforts(searchStartDate, searchEndDate, cat, stat);
                int columnCount = gui.getUpperTable().getModel().getColumnCount();
                String[] data = new String[columnCount];
                //Entferne TableModelListener, solange Tabelle aktualisiert wird
                try {
                    gui.getUpperTable().getModel().removeTableModelListener(tableListener);
                } catch (Exception et) {
                    et.printStackTrace();
                }
                statusBuffer.clear();
                try {
                    while (rsAllEfforts.next()) {
                        //hh:mm berechnen
                        int minGes = rsAllEfforts.getInt(1);
                        HH_MM_Formatter.formatHourMinute(minGes);                    
                        data[0] =  HH_MM_Formatter.formatHourMinute(minGes);
                        
                        //restliche Felder
                        for (int i = 2 ; i <= columnCount ; i++) {
                            data[i-1] = rsAllEfforts.getString(i);
                            if (i == columnCount) {
                                statusBuffer.add(rsAllEfforts.getString(columnCount));
                                //System.out.println(rsAllEfforts.getString(columnCount));
                            }
                        }
                        upperTableModel.addRow(data);
                    }
                    //TableModelListener wieder zuschalten:
                    gui.getUpperTable().getModel().addTableModelListener(tableListener);
                } catch (SQLException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                
                //Gesamtzeit anzeigen
                try {
                    ResultSet rsTotalTime = dbConnector.selectTotalTime(searchStartDate, searchEndDate, cat, stat);
                    gui.setBorderLabel("Group by Order/Suborder | Total time " + HH_MM_Formatter.formatHourMinute(rsTotalTime.getInt(1)));  
                } catch (SQLException e2) {
                    // TODO Auto-generated catch block
                    e2.printStackTrace();
                }
                        
            }           
        });
    }
    
    private void initUpperTable() {
        
        //Hinzufügen ComboBox für Status
        for (int i = 1 ; i < gui.getStatList().getModel().getSize() ; i++) {
            statusBox.addItem(gui.getStatList().getModel().getElementAt(i));
        }
        DefaultCellEditor statusEditor = new DefaultCellEditor(statusBox);
        gui.getUpperTable().getColumnModel().getColumn(6).setCellEditor(statusEditor);
        
        //Mouse Listener für Anzeige lowerTable nach Auswahl
        gui.getUpperTable().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
  
                int selectedRow = gui.getUpperTable().getSelectedRow();     //selectedRow im VIEW
                int selectedRowModel = gui.getUpperTable().convertRowIndexToModel(selectedRow);     //selectedRow im MODEL
                
                String orderNr = (String) gui.getUpperTable().getModel().getValueAt(selectedRowModel, 1);
                String subOrderNr = (String) gui.getUpperTable().getModel().getValueAt(selectedRowModel, 4);
                String status = (String) gui.getUpperTable().getModel().getValueAt(selectedRowModel, 6);
                System.out.println(orderNr + " ; " + subOrderNr + " ; " + status);
                ResultSet rsEfforts = dbConnector.selectEfforts(searchStartDate, searchEndDate, orderNr, subOrderNr, status);
                
                int columnCount = gui.getUpperTable().getModel().getColumnCount();
                String[] data = new String[columnCount];  
                DefaultTableModel lowerTableModel = (DefaultTableModel) gui.getLowerTable().getModel();
                lowerTableModel.setRowCount(0);
                try {
                    while (rsEfforts.next()) {
                        for (int i = 1 ; i <= columnCount ; i++) {
                            if (i == 5) {   //Spalte Zeit -> Passe Format at
                                int minGes = rsEfforts.getInt("sum");         
                                data[4] =  HH_MM_Formatter.formatHourMinute(minGes);
                            } else {
                                data[i-1] = rsEfforts.getString(i);
                            }
                        }
                        lowerTableModel.addRow(data);
                    }
                    gui.getCenterLowerPanel().setBorder(BorderFactory.createCompoundBorder(
                            BorderFactory.createEmptyBorder(5, 5, 5, 5),
                            BorderFactory.createTitledBorder("Details (" + orderNr + " ; " + subOrderNr + " ; " + status + ")")));
                    
                } catch (SQLException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
            } 
        });
    }

    private void initLowerTable() {
        // TODO Auto-generated method stub
        
    }
    
    private class StatusChangeTableModelListener implements TableModelListener {
        @Override
        public void tableChanged(TableModelEvent e) {
            int row = e.getFirstRow();
            int column = e.getColumn();
            if (column != 6 ) {
                return;
            }
                        
            DefaultTableModel model = (DefaultTableModel)e.getSource();
            String statusOld = statusBuffer.get(row);
            String statusNew = (String) model.getValueAt(row, 6);  
            System.out.println(row + " ; " + column + " ; " + statusOld + " ; " + statusNew);
            
            int selectedRow = gui.getUpperTable().getSelectedRow();     //selectedRow im VIEW
            int selectedRowModel = gui.getUpperTable().convertRowIndexToModel(selectedRow);     //selectedRow im MODEL
            String orderNr = (String) gui.getUpperTable().getModel().getValueAt(selectedRowModel, 1);
            String subOrderNr = (String) gui.getUpperTable().getModel().getValueAt(selectedRowModel, 4);
            String status = (String) gui.getUpperTable().getModel().getValueAt(selectedRowModel, 6);
            dbConnector.updateStatus(searchStartDate, searchEndDate, orderNr, subOrderNr, statusOld, statusNew);
             

        }        
    }   
}
